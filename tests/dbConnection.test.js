const expect = require("chai").expect;
const mongoose = require('mongoose');
const { DB_URI_TEST } = require('../config/config');
const env = process.env.NODE_ENV || 'development';

describe("DB connection", () => {
    it("should connect and disconnect to mongodb", async () => {
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
        expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
        expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
        expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect(DB_URI_TEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });
    });
});
