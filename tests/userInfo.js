class User{
    constructor(firstName, lastName, email, birthdate, password) {
        this.firstName_ = firstName;
        this.lastName_ = lastName;
        this.email_ = email;
        this.birthdate_ = birthdate;
        this.password_ = password;
    }

    get firstName(){
        return this.firstName_;
    }

    get lastName(){
        return this.lastName_;
    }

    get email(){
        return this.email_;
    }

    get birthdate(){
        return this.birthdate_;
    }

    get password(){
        return this.password_;
    }
}

user1 = new User("test1", "test11", "test1@test.com", "01/01/1999", "password1");
user2 = new User("test2", "test21", "test2@test.com", "01/01/1999", "password1");
user3 = new User("test3", "test31", "test3@test.com", "01/01/1999", "password1");
user4 = new User("test4", "test41", "test4@test.com", "01/01/1999", "password1");
user5 = new User("test5", "test51", "test5@test.com", "01/01/1999", "password1");
user6 = new User("test6", "test61", "test6@test.com", "01/01/1999", "password1");

module.exports = {user1, user2, user3, user4, user5, user6};