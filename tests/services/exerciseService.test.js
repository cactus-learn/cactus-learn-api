const expect = require("chai").expect;
const mongoose = require('mongoose');
const Exercise = require("../../app/models/exercise");
const UserResponse = require("../../app/models/userResponse");
const User = require("../../app/models/user")
const Organisation = require("../../app/models/organisation")
const Groupe = require("../../app/models/groupe")
const {createGrp, addMemberGrp} = require("../../app/services/groupeService");
const {user1, user2, user3, user5} = require("../userInfo");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {updateUserRoleToCreator} = require("../../app/services/adminRequestService");
const {sendRequestToBecomeCreator} = require("../../app/services/userService");
const {create, addMember} = require("../../app/services/organisationService")
const {createExercise, getUserResponseForExercise, getMyExercises, getMyResponses,
deleteExercise, updateExercise, getExercisesByGroupe, getAllUserExercises, getGroupeList} = require("../../app/services/exerciseService");
const {createUserResponse, addCorrectionToUserResponse, getUserMarksForGroupe} = require("../../app/services/userResponseService")
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ROLE} = require("../../app/models/role")
const {GroupeError, OrganisationError,ExerciseError, UserDoesntExistError, UserError, UserNotAuthorizedError } = require('../../app/error/CustomError');

describe("test exercices and userResponse services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useU, nifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await Exercise.deleteMany({})
        await User.deleteMany({})
        await Organisation.deleteMany({})
        await Groupe.deleteMany({})


        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user1Unverified._id)

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);

            await registerUser(user5.firstName, user5.lastName, user5.email, user5.birthdate, user5.password);
            let user5Unverified = await User.findOne({email: user5.email.toLowerCase()})
            await verifyUser(user5Unverified.tokens[user5Unverified.tokens.length - 1].token);
            user5Unverified.role = ROLE.admin;
            user5Unverified.save()
            updateUserRoleToCreator(user5Unverified._id, user1Unverified._id)

            await create("Organisation1", user1Unverified._id)
            let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
            let body = {organisationId: organisation._id, name: "Groupe1", description: "groupe de test"}
            await createGrp(user1Unverified._id, body);
        } catch(error) {
            console.log(error)
        }
    })
    after(async() => {
        //mongoose.disconnect()
    });
    
    describe("test create exercice service", () => {    
        it("should create exercice and save it the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                await addMemberGrp(organisation._id, groupe._id, user2.email.toLowerCase(), user._id)
                await addMember(organisation._id, user2.email.toLowerCase(), "member", user._id)
                let body = {title: "exercice1", groupeId: groupe._id, startDate: Date.now(), endDate: Date.now(), 
                coefficient: 2, questions: [{text: "question1", questionType: "qcm", coefficient: 2, responses: [{text: "response1", isAnswer: true}]}]}
                let value = await createExercise(body, user._id);
                expect(value).to.have.property("creator", user._id)
                expect(value).to.have.property("groupeId", groupe._id)
                expect(value).to.have.property("title", "exercice1")
                expect(value).to.have.property("coefficient", 2)
                expect(value).to.have.property("total")
                expect(value).to.have.property("questions")
                expect(value).to.have.property("groupeId")
                expect(value).to.have.property("startDate")
                expect(value).to.have.property("endDate")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create exercice and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let body = {title: "exercice1", groupeId: groupe._id, startDate: Date.now(), endDate: Date.now(), 
                coefficient: 2, questions: [{text: "question1", questionType: "qcm", coefficient: 2, responses: [{text: "response1", isAnswer: true}]}]}
                let value = await createExercise(body, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test create response service", () => {    
        it("should create response and save it the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})

                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})

                let body = {exerciseId: exercice._id, userId: user._id, marks: 0, coefficient: 2, 
                    total: 3, sendAt: Date.now(), questions: [{text: "question1", questionType: "qcm", coefficient: 2, 
                responses: [{text: "response1", isAnswer: true, isUserResponse: true, compositionResponse: "composition"}]}]}

                let value = await createUserResponse(user._id, body);
                expect(value).to.have.property("exerciseId")
                expect(value).to.have.property("userId")
                expect(value).to.have.property("marks")
                expect(value).to.have.property("coefficient")
                expect(value).to.have.property("total")
                expect(value).to.have.property("sendAt")
                expect(value).to.have.property("questions")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create response and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})

                let body = {exerciseId: mongoose.Types.ObjectId(), userId: user._id, marks: 0, coefficient: 2, 
                    total: 3, sendAt: Date.now(), questions: [{text: "question1", questionType: "qcm", coefficient: 2, 
                responses: [{text: "response1", isAnswer: true, isUserResponse: true, compositionResponse: "composition"}]}]}

                let value = await createUserResponse(user._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ExerciseError)
                expect(error.message).to.be.equal("Exercise not found")
            }
        });
    });

    describe("test addCorrectionToUserResponse service", () => {    
        it("should return a json representing the exercices and the responses", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})

                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let userResponse = await UserResponse.findOne({exerciseId: exercice._id, userId: user._id})
                let body = {_id: userResponse._id, exerciseId: exercice._id, userId: user._id, marks: 0, coefficient: 2, 
                    total: 3, sendAt: Date.now(), questions: [{text: "question1", questionType: "qcm", coefficient: 2, 
                responses: [{text: "response1", isAnswer: true, isUserResponse: true, compositionResponse: "composition"}]}]}

                let value = await addCorrectionToUserResponse(user._id, body);
                expect(value).to.be.equal("Correction added.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw an exception ExerciseError User response not found", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})

                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})

                let userResponse = await UserResponse.findOne({exerciseId: exercice._id, userId: user._id})
                let body = {_id: mongoose.Types.ObjectId(), exerciseId: exercice._id, userId: user._id, marks: 0, coefficient: 2, 
                    total: 3, sendAt: Date.now(), questions: [{text: "question1", questionType: "qcm", coefficient: 2, 
                responses: [{text: "response1", isAnswer: true, isUserResponse: true, compositionResponse: "composition"}]}]}

                let value = await addCorrectionToUserResponse(user._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ExerciseError)
                expect(error.message).to.be.equal("User response not found")
            }
        });

        it("should not return anything and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let secondUser = await User.findOne({email: user2.email.toLowerCase()})

                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})

                let userResponse = await UserResponse.findOne({exerciseId: exercice._id, userId: user._id})
                let body = {_id: userResponse._id, exerciseId: exercice._id, userId: user._id, marks: 0, coefficient: 2, 
                    total: 3, sendAt: Date.now(), questions: [{text: "question1", questionType: "qcm", coefficient: 2, 
                responses: [{text: "response1", isAnswer: true, isUserResponse: true, compositionResponse: "composition"}]}]}

                let value = await addCorrectionToUserResponse(secondUser._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User can't add a comment")
            }
        });
        
    });


    describe("test get exercice by Id service", () => {    
        it("should return a json with exercice title, id and responses", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await getUserResponseForExercise(exercice._id, user._id);
                expect(value).to.have.property("_id")
                expect(value).to.have.property("title", "exercice1")
                expect(value).to.have.property("userResponses")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await getUserResponseForExercise(exercice._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not return anything and throw an exception ExerciseError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await getUserResponseForExercise(mongoose.Types.ObjectId(), user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ExerciseError)
                expect(error.message).to.be.equal("Exercise not found")
            }
        });

        it("should not return anything and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let secondUser = await User.findOne({email: user2.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await getUserResponseForExercise(exercice._id, secondUser._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User not allowed to answer")
            }
        });
    })

   describe("test getMyExercises service", () => {    
        /*it("should return an non empty array", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getMyExercises(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                console.log(error)
                expect(error).to.be.null
            }
        });*/
        
        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let value = await getMyExercises(mongoose.Types.ObjectId());
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should return an empty array", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getMyExercises(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test getMyResponses service", () => {    
        /*it("should return an non empty array", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getMyResponses(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                console.log(error)
                expect(error).to.be.null
            }
        });*/
        
        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let value = await getMyResponses(mongoose.Types.ObjectId());
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should return an empty array", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await getMyResponses(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });


    describe("test getExercisesByGroupe service", () => {    
        it("should return a json with exercice groupeName, exercices and responses", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getExercisesByGroupe(groupe._id, user._id);
                expect(value).to.have.property("groupeName", groupe.name)
                expect(value).to.have.property("exercises")
                expect(value).to.have.property("responses")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let value = await getExercisesByGroupe(groupe._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not return anything and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let secondUser = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getExercisesByGroupe(groupe._id, secondUser._id);
        
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User not allowed to see the exercise")
            }
        });
    })


    describe("test getAllUserExercises service", () => {    
        it("should return a json with exercice due, finished", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllUserExercises(user._id);
                expect(value).to.have.property("due")
                expect(value).to.have.property("finished")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })

    describe("test getGroupeList service", () => {    
        it("should return non empty array", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await getGroupeList(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return en empty array", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getGroupeList(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })

    describe("test updateExercise service", () => {    
        it("should not update an exercice in the database and thrown an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let body = {title: "exercice1Bis", groupeId: groupe._id, startDate: Date.now(), endDate: Date.now(), 
                coefficient: 2, questions: [{text: "question1", questionType: "qcm", coefficient: 2, responses: [{text: "response1", isAnswer: true}]}]}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await updateExercise(body, exercice._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not update an exercice in the database and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let body = {title: "exercice1Bis", groupeId: groupe._id, startDate: Date.now(), endDate: Date.now(), 
                coefficient: 2, questions: [{text: "question1", questionType: "qcm", coefficient: 2, responses: [{text: "response1", isAnswer: true}]}]}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let secondUser = await User.findOne({email: user2.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await updateExercise(body, exercice._id, secondUser._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User can't modify this exercise")
            }
        });

        it("should update an exercice in the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let body = {title: "exercice1Bis", groupeId: groupe._id, startDate: Date.now(), endDate: Date.now(), 
                coefficient: 2, questions: [{text: "question1", questionType: "qcm", coefficient: 2, responses: [{text: "response1", isAnswer: true}]}]}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1", groupeId: groupe._id, creator: user._id})
                let value = await updateExercise(body, exercice._id, user._id);
                expect(value).to.be.equal("Exercice updated.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })

    describe("test getAllUserExercises service", () => {    
        it("should return a json with exercice due, finished", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllUserExercises(user._id);
                expect(value).to.have.property("due")
                expect(value).to.have.property("finished")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })

    describe("test deleteExercise service", () => {    
        it("should not delete the exercice and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1Bis", groupeId: groupe._id, creator: user._id})
                let secondUser = await User.findOne({email: user2.email.toLowerCase()})
                let value = await deleteExercise(exercice._id, secondUser._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User can't delete this exercise")
            }
        });

        it("should delete the exercice and return a message saying Exercice deleted.", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let exercice = await Exercise.findOne({title: "exercice1Bis", groupeId: groupe._id, creator: user._id})
                let value = await deleteExercise(exercice._id, user._id);
                expect(value).to.be.equal("Exercice deleted.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })

});
