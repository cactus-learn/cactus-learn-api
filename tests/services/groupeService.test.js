const expect = require("chai").expect;
const mongoose = require('mongoose');
const Groupe = require("../../app/models/groupe");
const User = require("../../app/models/user");
const Organisation = require("../../app/models/organisation");
const {createGrp, deleteGrp, transfertGrpOwnership, addMemberGrp, removeMemberGrp, 
    getAllOrganisationGroupe, getAllGroupeWhereUserIsCreator, isAdmin, isMember, getAllGroupeWhereUserIsStudent} = require("../../app/services/groupeService");
const {user1, user2, user3, user5} = require("../userInfo");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {updateUserRoleToCreator} = require("../../app/services/adminRequestService");
const {sendRequestToBecomeCreator} = require("../../app/services/userService");
const {create} = require("../../app/services/organisationService")
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ROLE} = require("../../app/models/role")
const {GroupeError, OrganisationError, UserDoesntExistError, UserError, UserNotAuthorizedError } = require('../../app/error/CustomError');

describe("test groupe services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await Organisation.deleteMany({});
        await Groupe.deleteMany({});
        await User.deleteMany({})

        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user1Unverified._id)

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);

            await registerUser(user5.firstName, user5.lastName, user5.email, user5.birthdate, user5.password);
            let user5Unverified = await User.findOne({email: user5.email.toLowerCase()})
            await verifyUser(user5Unverified.tokens[user5Unverified.tokens.length - 1].token);
            user5Unverified.role = ROLE.admin;
            user5Unverified.save()
            updateUserRoleToCreator(user5Unverified._id, user1Unverified._id)

            await create("Organisation1", user1Unverified._id)
            await create("Organisation2", user3Unverified._id)
        }catch(error) {
            console.log(error)
        }
    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test create groupe service", () => {    
        it("should create a groupe and save it the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let body = {organisationId: organisation._id, name: "Groupe1", description: "groupe de test"}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createGrp(user._id, body);
                expect(value).to.have.property("name","Groupe1")
                expect(value).to.have.property("description","groupe de test")
                expect(value).to.have.property("organisationId", organisation._id)
                expect(value).to.have.property("creatorId", user._id)
                expect(value).to.have.property("students")
                expect(value).to.have.property("createdAt")
                expect(value).to.have.property("exercises")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create a groupe and throw an exception GroupeError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let body = {organisationId: organisation._id, name: "Groupe1", description: "groupe de test"}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createGrp(user._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("The groupe name is already taken.")
            }
        });
        
        it("should not create a groupe and throw an exception OrganisationError", async () => {
            try {
                let body = {organisationId: mongoose.Types.ObjectId(), name: "Groupe1", description: "groupe de test"}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createGrp(user._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("Organisation not found")
            }
        });

        it("should not create a groupe and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let body = {organisationId: organisation._id, name: "Groupe1", description: "groupe de test"}
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createGrp(mongoose.Types.ObjectId(), body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test getAllOrganisationGroupe service", () => {    
        it("should return an json containing all the groupe for an organisation", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllOrganisationGroupe(organisation._id, user._id);
                expect(value).to.have.property("_id")
                expect(value).to.have.property("name")
                expect(value).to.have.property("groupes").to.be.instanceOf(Array)
                expect(value).to.have.property("isAdmin")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return an empty array", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation2".toLowerCase()})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllOrganisationGroupe(organisation._id, user._id);
                expect(value).to.have.property("_id")
                expect(value).to.have.property("name")
                expect(value).to.have.property("groupes").to.be.instanceOf(Array)
                expect(value).to.have.property("isAdmin")
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not return an json and throw an exception OrganisationError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllOrganisationGroupe(mongoose.Types.ObjectId(), user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("Organisation not found")
            }
        });

        it("should not return an json and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let value = await getAllOrganisationGroupe(organisation._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test getAllGroupeWhereUserIsCreator service", () => {    
        it("should return an array not empty", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllGroupeWhereUserIsCreator(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return an empty array", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await getAllGroupeWhereUserIsCreator(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return an empty array", async () => {
            try {
                let value = await getAllGroupeWhereUserIsCreator(mongoose.Types.ObjectId());
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test transfert groupe ownership service", () => {    
        it("should transfert the ownership to the new owner and return a message saying The ownership has been transfered.", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let owner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertGrpOwnership(organisation._id, groupe._id, newOwner._id, owner._id);
                expect(value).to.be.equal("The ownership has been transfered.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not transfert the ownership to the new owner and throw an UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let owner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertGrpOwnership(organisation._id, groupe._id, newOwner._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not transfert the ownership to the new owner and throw an UserError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let owner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertGrpOwnership(organisation._id, groupe._id, mongoose.Types.ObjectId(), owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("New owner not found")
            }
        });

        it("should not transfert the ownership to the new owner and throw an GroupeError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let owner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertGrpOwnership(organisation._id, mongoose.Types.ObjectId(), newOwner._id, owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("The groupe does not exist")
            }
        });

        it("should not transfert the ownership to the new owner and throw an UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let owner = await User.findOne({email: user3.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertGrpOwnership(organisation._id, groupe._id, newOwner._id, owner._id,);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the groupe.")
            }
        });

        it("should not transfert the ownership to the new owner and throw an OrganisationError", async () => {
            try {
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let owner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertGrpOwnership(mongoose.Types.ObjectId(), groupe._id,  newOwner._id, owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("Organisation not found")
            }
        });
    })

    describe("test add member to groupe service", () => {    
        it("should add the new member to the groupe and return a message saying The member has been added.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await addMemberGrp(organisation._id, groupe._id, user1.email.toLowerCase(), user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not add the new member to the groupe and throw an exception UserDoesntExistError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await addMemberGrp(organisation._id, groupe._id, user1.email.toLowerCase(), mongoose.Types.ObjectId())
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not add the new member to the groupe and throw an exception UserError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await addMemberGrp(organisation._id, groupe._id, "test11@test.com", user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("New member not found")
            }
        });

        it("should not add the new member to the groupe and throw an exception OrganisationError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await addMemberGrp(mongoose.Types.ObjectId(), groupe._id, user1.email.toLowerCase(), user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("Organisation not found")
            }
        });

        it("should not add the new member to the groupe and throw an exception GroupeError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let value = await addMemberGrp(organisation._id, mongoose.Types.ObjectId(), user1.email.toLowerCase(), user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("Groupe not found")
            }
        });

        it("should not add the new member to the groupe and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMemberGrp(organisation._id, groupe._id, user1.email.toLowerCase(), user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the groupe.")
            }
        });

        it("should not add the new member to the groupe and throw an exception UserError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMemberGrp(organisation._id, groupe._id, user1.email.toLowerCase(), user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("The new members has already already been added.")
            }
        });
    });

    describe("test getAllGroupeWhereUserIsStudent service", () => {    
        it("should return a non null object representing all the groupes", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllGroupeWhereUserIsStudent(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return an empty array", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getAllGroupeWhereUserIsStudent(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not return anything and throw UserDoesntExistError" , async () => {
            try {
                let value = await getAllGroupeWhereUserIsStudent(mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test is member of the groupe service", () => {   
        it("should throw an exception UserDoesntExistError", async () => {
            try {
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await isMember(groupe._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should throw an exception GroupeError", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await isMember(mongoose.Types.ObjectId(), owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("The groupe does not exist.")
            }
        });
        
        it("should return true", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await isMember(groupe._id, owner._id);
                expect(value).to.be.equal(true)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return false", async () => {
            try {
                let member = await User.findOne({email: user3.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await isMember(groupe._id, member._id);
                expect(value).to.be.equal(false)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })


    describe("test is Admin service", () => {   
        it("should throw an exception UserDoesntExistError", async () => {
            try {
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await isAdmin(groupe._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should throw an exception GroupeError", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await isAdmin(mongoose.Types.ObjectId(), owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("The groupe does not exist.")
            }
        });
        
        it("should return true", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await isAdmin(groupe._id, owner._id);
                expect(value).to.be.equal(true)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return false", async () => {
            try {
                let member = await User.findOne({email: user3.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await isAdmin(groupe._id, member._id);
                expect(value).to.be.equal(false)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })


    describe("test remove member from groupe service", () => {    
        it("should not remove the member from the groupe and throw an exception UserDoesntExistError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMemberGrp(organisation._id, groupe._id, member._id, mongoose.Types.ObjectId())
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not remove the member from the groupe and throw an exception UserError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMemberGrp(organisation._id, groupe._id, mongoose.Types.ObjectId(), user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("Member not found")
            }
        });

        it("should not remove the member from the groupe and throw an exception OrganisationError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMemberGrp(mongoose.Types.ObjectId(), groupe._id, member._id, user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist")
            }
        });

        it("should not remove the member from the groupe and throw an exception GroupeError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMemberGrp(organisation._id, mongoose.Types.ObjectId(), member._id, user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("The groupe does not exist")
            }
        });

        it("should not remove the member from the groupe and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user2.email.toLowerCase()})
                let value = await removeMemberGrp(organisation._id, groupe._id, member._id, user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the groupe.")
            }
        });

        it("should remove the member from the groupe and return a message saying The user has been deleted from the groupe.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMemberGrp(organisation._id, groupe._id, member._id, user._id)
                expect(value).to.be.equal("The user has been deleted from the groupe.")
            } catch(error) {
                expect(error).to.be.null
            }
        });


        it("should not remove the member from the groupe and throw an exception UserError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let member = await User.findOne({email: user3.email.toLowerCase()})
                let value = await removeMemberGrp(organisation._id, groupe._id, member._id, user._id)
                expect(value).to.be.equal("The member has been added.")
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("The user is not part of the groupe.")
            }
        });
    });

    describe("test delete groupe service", () => {   
        it("should not delete the groupe and throw an exception GroupeError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await deleteGrp(mongoose.Types.ObjectId(), user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(GroupeError)
                expect(error.message).to.be.equal("The groupe does not exist.")
            }
        });
        
        it("should not delete the groupe and throw an exception UserDoesntExistError", async () => {
            try {
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await deleteGrp(groupe._id, mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not delete a groupe and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await deleteGrp(groupe._id, user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the groupe.")
            }
        });

        it("should delete a groupe from the database", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await deleteGrp(groupe._id, user._id)
                expect(value).to.be.equal("The groupe has been deleted.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })
});