const expect = require("chai").expect;
const mongoose = require('mongoose');
const Forum = require("../../app/models/forum");
const Post = require("../../app/models/post")
const User = require("../../app/models/user")
const Organisation = require("../../app/models/organisation")
const Groupe = require("../../app/models/groupe")
const {createGrp, addMemberGrp} = require("../../app/services/groupeService");
const {user1, user2, user3, user5} = require("../userInfo");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {updateUserRoleToCreator} = require("../../app/services/adminRequestService");
const {sendRequestToBecomeCreator} = require("../../app/services/userService");
const {create} = require("../../app/services/organisationService")
const {createForum, deleteForum, closeForum, openForum, ratePost, 
    createPost, getAllForumForGroupe, getAllPostForForum, getAllForumWithoutGroupe} = require("../../app/services/forumService");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ROLE} = require("../../app/models/role")
const {GroupeError, OrganisationError,ForumError, UserDoesntExistError, UserError, UserNotAuthorizedError } = require('../../app/error/CustomError');

describe("test forum services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await Forum.deleteMany({})
        await Post.deleteMany({})
        await User.deleteMany({})
        await Organisation.deleteMany({})
        await Groupe.deleteMany({})


        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user1Unverified._id)

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);

            await registerUser(user5.firstName, user5.lastName, user5.email, user5.birthdate, user5.password);
            let user5Unverified = await User.findOne({email: user5.email.toLowerCase()})
            await verifyUser(user5Unverified.tokens[user5Unverified.tokens.length - 1].token);
            user5Unverified.role = ROLE.admin;
            user5Unverified.save()
            updateUserRoleToCreator(user5Unverified._id, user1Unverified._id)

            await create("Organisation1", user1Unverified._id)
            let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
            let body = {organisationId: organisation._id, name: "Groupe1", description: "groupe de test"}
            await createGrp(user1Unverified._id, body);

            let body2 = {organisationId: organisation._id, name: "Groupe2", description: "groupe de test"}
            await createGrp(user2Unverified._id, body2);
        } catch(error) {
            console.log(error)
        }


    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test create forum service", () => {    
        it("should create a forum and save it the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createForum("Forum1", groupe._id, organisation._id, user._id);
                expect(value).to.have.property("name","Forum1".toUpperCase())
                expect(value).to.have.property("groupeId", groupe._id)
                expect(value).to.have.property("organisationId", organisation._id)
                expect(value).to.have.property("creatorId", user._id)
                expect(value).to.have.property("posts")
                expect(value).to.have.property("statut_close")
                expect(value).to.have.property("createdAt")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create a forum and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createForum("Forum1", groupe._id, organisation._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not create a forum and throw an exception ForumError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createForum("Forum1", groupe._id, organisation._id, user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("The forum already exist.")
            }
        });
    });


    describe("test close forum service", () => {    
        it("should close a forum and update the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await closeForum(forum._id, user._id);
                expect(value).to.be.equal("The forum has been closed")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not close the forum and return a message saying the forum does not exist", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await closeForum(forum._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not close the forum and throw an exception ForumError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await closeForum(mongoose.Types.ObjectId(), user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("This forum does not exist")
            }
        });

        it("should not close the forum and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await closeForum(forum._id, user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the forum")
            }
        });
    })

    describe("test open forum service", () => {    
        it("should open a forum and update the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await openForum(forum._id, user._id);
                expect(value).to.be.equal("The forum has been opened")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not open the forum and thrown an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await openForum(forum._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not open the forum and throw an exception ForumError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await openForum(mongoose.Types.ObjectId(), user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("This forum does not exist")
            }
        });

        it("should not open the forum and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await openForum(forum._id, user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the forum")
            }
        });
    })

    describe("test getAllForumForGroupe service", () => {    
        it("should return an array not empty", async () => {
            try {
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let value = await getAllForumForGroupe(groupe._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should return an empty array", async () => {
            try {
                let groupe = await Groupe.findOne({name: "Groupe2"})
                let value = await getAllForumForGroupe(groupe._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test getAllForumWithoutGroupe service", () => {    
        it("should return an empty array", async () => {
            try {
                let value = await getAllForumWithoutGroupe();
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test create post service", () => {    
        it("should create a post and return it", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createPost(forum._id, "Hello world!", user._id);
                expect(value).to.have.property("post").to.have.property("forumId", forum._id)
                expect(value).to.have.property("post").to.have.property("creatorId", user._id)
                expect(value).to.have.property("post").to.have.property("content", "Hello world!")
                expect(value).to.have.property("post").to.have.property("thumb_up")
                expect(value).to.have.property("post").to.have.property("thumb_down")
                expect(value).to.have.property("post").to.have.property("createdAt")
                expect(value).to.have.property("userInfo").to.have.property("_id")
                expect(value).to.have.property("userInfo").to.have.property("firstName")
                expect(value).to.have.property("userInfo").to.have.property("lastName")
                expect(value).to.have.property("userInfo").to.have.property("email")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create a post and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createPost(forum._id, "Hello world!", mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not create a post and throw an exception ForumError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createPost(mongoose.Types.ObjectId(), "Hello world!", user._id);
                expect(value).to.not.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("The forum does not exist.")
            }
        });
    });


    describe("test getAllPostForForum service", () => {    
        it("should return an array not empty", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let value = await getAllPostForForum(forum._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw an exception ForumError forum not found", async () => {
            try {
                let value = await getAllPostForForum(mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("Forum not found")
            }
        });
        
        it("should return an empty array ", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let user = await User.findOne({email: user3.email.toLowerCase()})
                try {
                    await createForum("Forum2", groupe._id, organisation._id, user._id);
                }catch(error){
                    console.log(error)
                }
                let forum = await Forum.findOne({name: "Forum2".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let value = await getAllPostForForum(forum._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });


    describe("test rate post service", () => {    
        it("should add the user id to the thumb up list", async () => {
            try{
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let postCreator = await User.findOne({email: user1.email.toLowerCase()})
                let post = await Post.findOne({creatorId: postCreator._id})
                let positif = true

                let value = await ratePost(post._id, positif, user._id);
                expect(value).to.be.equal("The number of thumb has been raised.")
                let voteUp = await Post.findOne({_id: post._id, 'thumb_up.userId': user._id})
                let voteDown = await Post.findOne({_id: post._id, 'thumb_down.userId': user._id})
                expect(voteUp).to.not.be.null
                expect(voteDown).to.be.null
            } catch (error) {
                expect(error).to.be.null
            }
        });

        
        it("should add the user id to the thumb down list", async () => {
            try{
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let postCreator = await User.findOne({email: user1.email.toLowerCase()})
                let post = await Post.findOne({creatorId: postCreator._id})
                let positif = false
                let value = await ratePost(post._id, positif, user._id);
                expect(value).to.be.equal("The number of thumb has been raised.")
                let voteUp = await Post.findOne({_id: post._id, 'thumb_up.userId': user._id})
                let voteDown = await Post.findOne({_id: post._id, 'thumb_down.userId': user._id})
                expect(voteUp).to.be.null
                expect(voteDown).to.not.be.null
            } catch (error) {
                expect(error).to.be.null
            }
        });


        it("should not the user id to any list and throw an exception ForumError post doest not exist", async () => {
            try{
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let postCreator = await User.findOne({email: user1.email.toLowerCase()})
                let post = await Post.findOne({creatorId: postCreator._id})
                let positif = false
                let value = await ratePost(mongoose.Types.ObjectId(), positif, user._id);
                expect(value).to.be.null
            } catch (error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("The post does not exist")
            }
        });

        it("should not the user id to any list and throw an exception ForumError Already voted", async () => {
            try{
                let postCreator = await User.findOne({email: user1.email.toLowerCase()})
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let post = await Post.findOne({creatorId: postCreator._id})
                let positif = false
                let value = await ratePost(post._id, positif, user._id);
                expect(value).to.be.null
            } catch (error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("Already voted")
            }
        });
    })

    describe("test delete forum service", () => {
        it("should not delete the forum and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let value = await deleteForum(forum._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not delete the forum and throw an exception ForumError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await deleteForum(mongoose.Types.ObjectId(), user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ForumError)
                expect(error.message).to.be.equal("This forum does not exist")
            }
        });
        
        it("should not delete the forum and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let value = await deleteForum(forum._id, user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator of the forum")
            }
        });

        it("should delete a forum and save it the database", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({name: "Groupe1"})
                let forum = await Forum.findOne({name: "Forum1".toUpperCase(), groupeId: groupe._id, organisationId: organisation._id})
                let value = await deleteForum(forum._id, user._id);
                expect(value).to.be.equal("The forum has been deleted")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })
});
