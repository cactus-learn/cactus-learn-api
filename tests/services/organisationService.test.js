const expect = require("chai").expect;
const mongoose = require('mongoose');
const Organisation = require("../../app/models/organisation");
const User = require("../../app/models/user");
const {create, getAllOrganisationUsers, getAllUserOrganisations, deleteOrg, getAllUsersFromOrginsationsWhereUserIsPresent,
    transfertOwnership, addMember, removeMember,IsMemberOfOrganisation, isAdmin} = require("../../app/services/organisationService")
const {user1, user2, user3, user4, user5} = require("../userInfo");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {updateUserRoleToCreator} = require("../../app/services/adminRequestService");
const {sendRequestToBecomeCreator} = require("../../app/services/userService");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ROLE} = require("../../app/models/role")
const {OrganisationError, UserDoesntExistError, UserError, UserNotAuthorizedError } = require('../../app/error/CustomError');

describe("test organisation services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await Organisation.deleteMany({});
        await User.deleteMany({})

        try{
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user1Unverified._id)

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);

            await registerUser(user5.firstName, user5.lastName, user5.email, user5.birthdate, user5.password);
            let user5Unverified = await User.findOne({email: user5.email.toLowerCase()})
            await verifyUser(user5Unverified.tokens[user5Unverified.tokens.length - 1].token);
            user5Unverified.role = ROLE.admin;
            user5Unverified.save()
            updateUserRoleToCreator(user5Unverified._id, user1Unverified._id)
        } catch(error) {
            console.log(error)
        }
    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test create organisation service", () => {    
        it("should create an organisation and save it the database", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await create("Brigade fantôme", user._id);
                expect(value).to.have.property("name","Brigade fantôme".toLocaleLowerCase())
                expect(value).to.have.property("creatorId",user._id)
                expect(value).to.have.property("admins")
                expect(value).to.have.property("volunteers")
                expect(value).to.have.property("members")
                expect(value).to.have.property("groupes")
                expect(value).to.have.property("createdAt")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create an organisation and throw an exception OrganisationError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await create("Brigade fantôme", user._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation name is already taken.")
            }
        });
        
        it("should not create an organisation and throw an exception UserDoesntExistError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await create("Brigade fantôme", mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });

    describe("test getAllUsersFromOrginsationsWhereUserIsPresent service", () => {    
        it("should return a non null object representing all the users", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllUsersFromOrginsationsWhereUserIsPresent(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw an exception OrganisationError", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getAllUsersFromOrginsationsWhereUserIsPresent(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let value = await getAllUsersFromOrginsationsWhereUserIsPresent(mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test get all user organisations service", () => {    
        it("should return an array not empty", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAllUserOrganisations(user._id);
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let value = await getAllUserOrganisations(mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });


    describe("test transfert organisation ownership service", () => {    
        it("should transfert the ownership to the new owner and return a message saying The ownership has been transfered.", async () => {
            try {
                let currentOwner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await transfertOwnership(organisation._id, newOwner._id, currentOwner._id);
                expect(value).to.be.equal("The ownership has been transfered.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not transfert the ownership to the new owner and throw an exception UserDoesntExistError", async () => {
            try {
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await transfertOwnership(organisation._id, newOwner._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not transfert the ownership to the new owner and throw an exception UserError", async () => {
            try {
                let currentOwner = await User.findOne({email: user1.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await transfertOwnership(organisation._id, mongoose.Types.ObjectId(), currentOwner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("New owner not found")
            }
        });

        it("should not transfert the ownership to the new owner and throw an exception UserError", async () => {
            try {
                let currentOwner = await User.findOne({email: user1.email.toLowerCase()})
                let newOwner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await transfertOwnership(mongoose.Types.ObjectId(), newOwner._id, currentOwner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist")
            }
        });
    })


    describe("test is member of organisation service", () => {   
        it("should throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await IsMemberOfOrganisation(organisation._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should throw an exception OrganisationError", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await IsMemberOfOrganisation(mongoose.Types.ObjectId(), owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist.")
            }
        });
        
        it("should return true", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await IsMemberOfOrganisation(organisation._id, owner._id);
                expect(value).to.be.equal(true)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return false", async () => {
            try {
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await IsMemberOfOrganisation(organisation._id, member._id);
                expect(value).to.be.equal(false)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })


    describe("test is Admin service", () => {   
        it("should throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await isAdmin(organisation._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should throw an exception OrganisationError", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await isAdmin(mongoose.Types.ObjectId(), owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist.")
            }
        });
        
        it("should return true", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await isAdmin(organisation._id, owner._id);
                expect(value).to.be.equal(true)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should return false", async () => {
            try {
                let member = await User.findOne({email: user1.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await isAdmin(organisation._id, member._id);
                expect(value).to.be.equal(false)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })
    

    describe("test add member to organisation service", () => {   
        it("should not add the new member and throw UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMember(organisation._id, user1.email.toLowerCase(), "admin", user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator or admin of the organisation.")
            }
        });

        it("should add the new member as admin and return a message saying The member has been added.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMember(organisation._id, user1.email.toLowerCase(), "admin", user._id)
                expect(value).to.have.property("_id")
                expect(value).to.have.property("firstName", newMember.firstName)
                expect(value).to.have.property("lastName", newMember.lastName)
                expect(value).to.have.property("email", newMember.email)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should add the new member as volunteer and return a message saying The member has been added.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user3.email.toLowerCase()})
                let value = await addMember(organisation._id, user3.email.toLowerCase(), "volunteer", user._id)
                expect(value).to.have.property("_id")
                expect(value).to.have.property("firstName", newMember.firstName)
                expect(value).to.have.property("lastName", newMember.lastName)
                expect(value).to.have.property("email", newMember.email)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should add the new member as member and return a message saying The member has been added.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user5.email.toLowerCase()})
                let value = await addMember(organisation._id, user5.email.toLowerCase(), "member", user._id)
                expect(value).to.have.property("_id")
                expect(value).to.have.property("firstName", newMember.firstName)
                expect(value).to.have.property("lastName", newMember.lastName)
                expect(value).to.have.property("email", newMember.email)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not add the new member as admin and throw an organisationError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMember(organisation._id, user1.email.toLowerCase(), "admin", user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The new members has already already been added as admin")
            }
        });

        it("should not add the new member as volunteer and throw an organisationError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user3.email.toLowerCase()})
                let value = await addMember(organisation._id, user3.email.toLowerCase(), "volunteer", user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The new members has already already been added as volunteer")
            }
        });

        it("should not add the new member as member and throw an organisationError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMember(organisation._id, user5.email.toLowerCase(), "member", user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The new members has already already been added as member")
            }
        });
 
        it("should not add the new member and throw an UserError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await addMember(organisation._id, "test11@test.com", "member", user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("New member not found")
            }
        });
        
        it("should not add the new member and throw an UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMember(organisation._id, user1.email.toLowerCase(), "member", mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not add the new member and throw an OrganisationError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await addMember(mongoose.Types.ObjectId(), user1.email.toLowerCase(), "member", user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist")
            }
        });
    });


    describe("test get all organisation users service", () => {    
        it("should return an json object containing all the members of the organisation", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await getAllOrganisationUsers(organisation._id);
                expect(value).to.have.property("members")
                expect(value).to.have.property("volunteers")
                expect(value).to.have.property("admins")
                expect(value).to.have.property("creator")
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not return anything and throw an exception UserDoesntExistError", async () => {
            try {
                let value = await getAllOrganisationUsers(mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("Organisation not found")
            }
        });
    });

    describe("test remove member from organisation service", () => {    
        it("should remove a member and return a message saying The user has been deleted from admins.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMember(organisation._id, newMember._id, user._id)
                expect(value).to.be.equal("The user has been deleted from admins.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should remove a member and return a message The user has been deleted from members.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let member = await User.findOne({email: user5.email.toLowerCase()})
                let value = await removeMember(organisation._id, member._id, user._id)
                expect(value).to.be.equal("The user has been deleted from members.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should remove a member and return a message The user has been deleted from volunteers.", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let member = await User.findOne({email: user3.email.toLowerCase()})
                let value = await removeMember(organisation._id, member._id, user._id)
                expect(value).to.be.equal("The user has been deleted from volunteers.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not remove a member and throw an UserError", async () => {
            await registerUser(user4.firstName, user4.lastName, user4.email, user4.birthdate, user4.password);
            let user4Unverified = await User.findOne({email: user4.email.toLowerCase()})
            await verifyUser(user4Unverified.tokens[user4Unverified.tokens.length - 1].token);
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await removeMember(organisation._id, user4Unverified._id, user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("The user is not part of the organisation.")
            }
        });

        it("should not remove a member and return a message saying The organisation does not exist", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMember(mongoose.Types.ObjectId(), newMember._id, user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist")
            }
        });

        it("should not remove a member and return a message saying User not found", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMember(organisation._id, newMember._id, mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not remove a member and return a message saying Member not found", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await removeMember(organisation._id, mongoose.Types.ObjectId(), user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("Member not found")
            }
        });

        it("should not remove a member and throw an UserNotAuthorizedError exception", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let newMember = await User.findOne({email: user1.email.toLowerCase()})
                let value = await removeMember(organisation._id, newMember._id, user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You must be the creator or admin of the organisation.")
            }
        });
    });


    describe("test delete organisation service", () => {   
        it("should not delete the organisation and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await deleteOrg(organisation._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not delete the organisation and throw an exception OrganisationError", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let value = await deleteOrg(mongoose.Types.ObjectId(), owner._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(OrganisationError)
                expect(error.message).to.be.equal("The organisation does not exist.")
            }
        });
        
        it("should delete the organisation and return a message saying Organisation deleted", async () => {
            try {
                let owner = await User.findOne({email: user2.email.toLowerCase()})
                let organisation = await Organisation.findOne({name: "Brigade fantôme"})
                let value = await deleteOrg(organisation._id, owner._id);
                expect(value).to.be.equal("Organisation deleted")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })
});