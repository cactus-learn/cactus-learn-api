const expect = require("chai").expect;
const mongoose = require('mongoose');
const User = require("../../app/models/user");
const {getAdminCreatorForUser, getCreatorRequestById, getAllCreatorRequests, updateUserRoleToCreator, denyUserCreatorRequest} = require("../../app/services/adminRequestService");
const {user1, user2, user3, user4} = require("../userInfo");
const {sendRequestToBecomeCreator} = require("../../app/services/userService");
const {registerUser, verifyUser} = require("../../app/services/authService");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const AdminRequest = require("../../app/models/adminRequest")
const {AdminRequestError, UserDoesntExistError, UserError } = require('../../app/error/CustomError');
const {ROLE} = require("../../app/models/role")

describe("test creator request services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await User.deleteMany({})
        await AdminRequest.deleteMany({})

        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user1Unverified._id)

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user2Unverified._id)

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);
            user3Unverified.role = ROLE.creator;
            user3Unverified.save()

            await registerUser(user4.firstName, user4.lastName, user4.email, user4.birthdate, user4.password);
            let user4Unverified = await User.findOne({email: user4.email.toLowerCase()})
            await verifyUser(user4Unverified.tokens[user4Unverified.tokens.length - 1].token);
        } catch(error) {
            console.log(error)
        }
    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test getAdminCreatorForUser service", () => {    
        it("should return a non empty array the user creator request", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getAdminCreatorForUser(user._id)
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw an exception UserError", async () => {
            try {
                let value = await getAdminCreatorForUser( mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("User not found")
            }
        });

        it("should not return anything and throw an exception AdminRequestError", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getAdminCreatorForUser(user._id)
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test getAllCreatorRequests service", () => {    
        it("should return an non empty array", async () => {
            try {
                let value = await getAllCreatorRequests()
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test updateUserRoleToCreator service", () => {    
        it("should not update the user role to creator and throw an exception UserError", async () => {
            try {
                let validator = await User.findOne({email: user3.email.toLowerCase()})
                let value = await updateUserRoleToCreator(validator._id, mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("User not found")
            }
        });

        it("should not update the user role to creator and throw an exception UserDoesntExistError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updateUserRoleToCreator(mongoose.Types.ObjectId(), user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not update the user role to creator and throw an exception AdminRequestError", async () => {
            try {
                let user = await User.findOne({email: user4.email.toLowerCase()})
                let validator = await User.findOne({email: user3.email.toLowerCase()})
                let value = await updateUserRoleToCreator(validator._id, user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(AdminRequestError)
                expect(error.message).to.be.equal("No creator requestion found for the user.")
            }
        });

        it("should update the user role to creator in the databse and return a message saying User role updated", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let validator = await User.findOne({email: user3.email.toLowerCase()})
                let value = await updateUserRoleToCreator(validator._id, user._id)
                console.log("Herre granted")
                expect(value).to.be.equal("Request granted.")
            } catch(error) {
                expect(error).to.not.be.null
            } 
        });
    });

    describe("test denyUserCreatorRequest service", () => {    
        it("should not update the user role to creator and throw an exception UserError", async () => {
            try {
                let validator = await User.findOne({email: user3.email.toLowerCase()})
                let value = await updateUserRoleToCreator(validator._id, mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("User not found")
            }
        });

        it("should not update the user role to creator and throw an exception UserDoesntExistError", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await updateUserRoleToCreator(mongoose.Types.ObjectId(), user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not update the user role to creator and throw an exception AdminRequestError", async () => {
            try {
                let user = await User.findOne({email: user4.email.toLowerCase()})
                let validator = await User.findOne({email: user3.email.toLowerCase()})
                let value = await updateUserRoleToCreator(validator._id, user._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(AdminRequestError)
                expect(error.message).to.be.equal("No creator requestion found for the user.")
            }
        });

        it("should denied the user request and return a message saying User creator request denied", async () => {
            try {
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let validator = await User.findOne({email: user3.email.toLowerCase()})
                let value = await updateUserRoleToCreator(validator._id, user._id)
                console.log("Herre deneid")
                expect(value).to.be.equal("Request denied")
            } catch(error) {
                expect(error).to.not.be.null
            } 
        });
    });
});