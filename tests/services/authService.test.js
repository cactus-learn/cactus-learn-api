const expect = require("chai").expect;
const mongoose = require('mongoose');
const User = require("../../app/models/user");
const {valideEmail, registerUser, verifyUser, resendVerificationToken, loginUser, logoutUser} = require("../../app/services/authService");
const {user1, user2} = require("../userInfo");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ValidationError, AuthError, UserNotAuthorizedError, UserDoesntExistError, UserError} = require("../../app/error/CustomError");

describe("test authentication services", function(){
    this.timeout(10000)
    before(async function() {
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/
        await User.deleteMany({})
    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test valideEmail service", () => {    
        it("should return true when the email is valide", async () => {
            let value = valideEmail(user1.email)
            expect(value).to.equal(true)
        });

        it("should return false when the email is not valide", async () => {
            let value = valideEmail("test1@test")
            expect(value).to.equal(false)
        });
    });


    describe("test register user service", () => {    
        it("should register the new user and return message saying the email sent", async () => {
            try {
                let value = await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
                expect(value).to.be.equal("Email sent")
            } catch(error) {
                expect(error).to.not.be.null
            }
        });
        
        it("should not register the new user and throw an error saying First name is required.", async () => {
            try {
                let value = await registerUser("", user1.lastName, user1.email, user1.birthdate, user1.password);
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("First name is required.")
            }
        });

        it("should not register the new user and return message saying Last name is required.", async () => {
            try {
                let value = await registerUser(user1.firstName, "", user1.email, user1.birthdate, user1.password);
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Last name is required.")
            }
        });

        it("should not register the new user and return message saying Email is required.", async () => {
            try {
                let value = await registerUser(user1.firstName, user1.lastName, "", user1.birthdate, user1.password);
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Email is required.")
            }
        });

        it("should not register the new user and return message saying Birthdate is required.", async () => {
            try {
                let value = await registerUser(user1.firstName, user1.lastName, user1.email, "", user1.password);
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Birthdate is required.")
            }
        });

        it("should not register the new user and return message saying Password is required.", async () => {
            try {
                let value = await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, "");
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Password is required.")
            }
        });

        it("should not register the new user and return message saying Email address is not valide.", async () => {
            try {
                let value = await registerUser(user1.firstName, user1.lastName, "test@test", user1.birthdate, user1.password);
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Email address is not valide.")
            }
        });

        it("should not register the new user and return message saying Email is already use", async () => {
            try {
                let value = await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(AuthError)
                expect(error.message).to.be.equal("Email is already use")
            }
        });
    });


    describe("test verify user service", () => {  
        it("should not verify the new user and return message saying Invalid token", async () => {
            try {
                let user = await User.findOne({email: user1.email})
                let value = await verifyUser(user.tokens[user.tokens.length - 1].token + "error");
                expect(value).to.not.be.equal("The user is now verified")
            } catch (error) {
                expect(error).to.be.instanceOf(AuthError)
                expect(error.message).to.be.equal("Invalid token")
            }

            let verifiedUser = await User.findOne({email: user1.email})
            expect(verifiedUser).to.have.property("verified", false)
        });

        it("should verify the new user and return message saying The user is now verified", async () => {
            try {
                let user = await User.findOne({email: user1.email})
                let value = await verifyUser(user.tokens[user.tokens.length - 1].token);
                expect(value).to.be.equal("The user is now verified")
            } catch (error) {
                expect(error).to.be.null
            }

            let verifiedUser = await User.findOne({email: user1.email})
            expect(verifiedUser).to.have.property("verified", true)
        });
    });

    describe("test resend verification token service", () => {    
        it("should send the verification mail and return message Email sent", async () => {
            try {
                let value = await resendVerificationToken(user1.email)
                expect(value).to.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not send the verification mail and return message Email is required.", async () => {
            try {
                let value = await resendVerificationToken("")
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Email is required.")
            }
        });

        it("should not send the verification mail and return message User not found", async () => {
            try {
                let value = await resendVerificationToken("test9@test.com")
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(AuthError)
                expect(error.message).to.be.equal("User not found")
            }
        });
    });


    describe("test login user service", () => {    
        it("should login the new user and return message saying login success and a token", async () => {
            try {
                let value = await loginUser(user1.email, user1.password);
                let user = await User.findOne({email: user1.email})
                expect(value).to.be.equal(user.tokens[user.tokens.length - 1].token)
            } catch(error) {
                expect(error).to.be.null
            }
        });
        
        it("should not login the new user and return message saying Password is required.", async () => {
            try {
                let value = await loginUser(user1.email, "");
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Password is required.")
            }
        });

        it("should not login the new user and return message saying Email is required.", async () => {
            try {
                let value = await loginUser("", user1.password);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Email is required.")
            }
        });

        it("should not login the new user and return message saying Email address is not valid.", async () => {
            try {
                let value = await loginUser("test1@test", user1.password);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(AuthError)
                expect(error.message).to.be.equal("Email address is not valid.")
            }
        });

        it("should not login the new user and return message saying Invalid credentials", async () => {
            try {
                let value = await loginUser("test9@test.com", user1.password);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not login the new user and return message saying Invalid credentials", async () => {
            try {
                let value = await loginUser(user1.email, "password4");
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("Invalid credentials")
            }
        });

        it("should not login the new user and return message saying Your account is not verify yet.", async () => {
            try {
                let message = await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
                expect(message).to.be.equal("Email sent")
                let value = await loginUser(user2.email, user2.password);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(AuthError)
                expect(error.message).to.be.equal("Your account is not verify yet.")
            }
        });
    });

    /*describe("test logout user service", () => {    
        it("should not login the user and return message user not found", async () => {
            let value = await logoutUser(mongoose.Types.ObjectId());
            expect(value).to.have.property("error_user", "User not found")
            expect(value).to.not.have.property("success", "The user has been logout")
        });
        
        it("should logout the user and return message saying The user has been logout", async () => {
            let user = await User.findOne({email: user2.email})
            let value = await logoutUser(user._id);
            expect(value).to.not.have.property("error_user", "User not found")
            expect(value).to.have.property("success", "The user has been logout")
        });
    });*/
});