const expect = require("chai").expect;
const mongoose = require('mongoose');
const User = require("../../app/models/user");
const {forgotPassword} = require("../../app/services/forgotPasswordService");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {user1} = require("../userInfo");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ValidationError, UserError} = require("../../app/error/CustomError");

describe("test forgot password services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/
        await User.deleteMany({})

        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
        } catch(error) {
            console.log(error)
        }
    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test forgot password service", () => {    
        it("should send an email with the reset link", async () => {
            try {
                let value = await forgotPassword(user1.email.toLowerCase())
                expect(value).to.be.equal("Email sent")
            } catch(error) {
                expect(error).to.not.be.null
            }
        });

        it("should not send an email with the reset link and return message saying the email address is required", async () => {
            try {
                let value = await forgotPassword(null)
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("Email address is required.")
            }
        });

        it("should not send an email with the reset link and return message saying the email address is not valid", async () => {
            try {
                let value = await forgotPassword("test1@test")
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(ValidationError)
                expect(error.message).to.be.equal("The email address is not valid")
            }
        });

        it("should not send an email with the reset link and return message saying the user is not found", async () => {
            try {
                let value = await forgotPassword("test9@test.com")
                expect(value).to.not.be.equal("Email sent")
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("User not found")
            }
        });
    });
});