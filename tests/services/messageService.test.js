const expect = require("chai").expect;
const mongoose = require('mongoose');
const User = require("../../app/models/user");
const Message = require("../../app/models/message");
const {sendMessage, updateIsLikedStatut, updateReadStatut, getAllMessagesBetweenTwoMembers, getUserConversations} = require("../../app/services/messageService");
const {user1, user2, user3} = require("../userInfo");
const {registerUser, verifyUser} = require("../../app/services/authService");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {MessageError, UserDoesntExistError, UserError, UserNotAuthorizedError } = require('../../app/error/CustomError');

describe("test messaging services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await User.deleteMany({})
        await Message.deleteMany({})

        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);
        } catch(error) {
            console.log(error)
        }
    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test send message service", () => {    
        it("should send a message and return saying message sent.", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let value = await sendMessage(receiver._id, "hello world",  user._id)
                expect(value).to.be.equal("Message sent.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not send a message and throw an exception", async () => {
            try {
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let value = await sendMessage(receiver._id, "hello world!",  mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("Error while retrieving the user")
            }
        });
    });

    describe("test updateReadStatut service", () => {    
        it("should not update a message read statut and throw an exception MessageError", async () => {
            try {
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let value = await updateReadStatut(mongoose.Types.ObjectId(), receiver._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(MessageError)
                expect(error.message).to.be.equal("Error while retrieving the message")
            }
        });

        it("should not update a message read statut and throw an exception UserNotAuthorizedError" , async () => {
            try {
                let sender = await User.findOne({email: user1.email.toLowerCase()})
                let message = await Message.findOne({sender: sender._id})
                let value = await updateReadStatut(message._id, sender._id)
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("You are the message sender")
            }
        });

        it("should update a message read statut and return saying Read statut updated.", async () => {
            try {
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let message = await Message.findOne({receiverId: receiver._id})
                let value = await updateReadStatut(message._id, receiver._id)
                expect(value).to.be.equal("Read statut updated.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test updateIsLikedStatut service", () => {    
        it("should not update a message like statut and throw an exception MessageError", async () => {
            try {
                let value = await updateIsLikedStatut(mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(MessageError)
                expect(error.message).to.be.equal("Error while retrieving the message")
            }
        });

        it("should update a message like statut and return saying Read statut updated.", async () => {
            try {
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let message = await Message.findOne({receiverId: receiver._id})
                let value = await updateIsLikedStatut(message._id)
                expect(value).to.be.equal("The message has been liked.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });


    describe("test getAllMessagesBetweenTwoMembers service", () => {    
        it("should return an array not empty", async () => {
            try {
                let userSender1 = await User.findOne({email: user1.email.toLowerCase()})
                let receiver1 = await User.findOne({email: user2.email.toLowerCase()})
                let value1 = await sendMessage(receiver1._id, "hello world too!",  userSender1._id)
                expect(value1).to.be.equal("Message sent.")

                let userSender2 = await User.findOne({email: user2.email.toLowerCase()})
                let receiver2 = await User.findOne({email: user1.email.toLowerCase()})
                let value2 = await sendMessage(receiver2._id, "hello world again!",  userSender2._id)
                expect(value2).to.be.equal("Message sent.")
                
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let value = await getAllMessagesBetweenTwoMembers(receiver._id,  user._id)
                
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return anything and throw and exception ", async () => {
            try {
                let receiver = await User.findOne({email: user2.email.toLowerCase()})
                let value = await getAllMessagesBetweenTwoMembers(receiver._id,  mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserError)
                expect(error.message).to.be.equal("Error while retrieving the user")
            }
        });

        it("should not return anything and throw and exception ", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let receiver = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getAllMessagesBetweenTwoMembers(receiver._id, user._id)
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });

    describe("test getUserConversations service", () => {    
        it("should return an array not empty", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getUserConversations(user._id)
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.greaterThan(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return an array and throw an exception UserDoesntExistError", async () => {
            try {
                let value = await getUserConversations(mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not return an empty array", async () => {
            try {
                let user = await User.findOne({email: user3.email.toLowerCase()})
                let value = await getUserConversations(user._id)
                expect(value).to.be.instanceOf(Array)
                expect(value.length).to.be.equal(0)
            } catch(error) {
                expect(error).to.be.null
            }
        });
    });
});