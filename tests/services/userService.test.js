const expect = require("chai").expect;
const mongoose = require('mongoose');
const User = require("../../app/models/user");
const AdminRequest = require("../../app/models/adminRequest")
const {getUserInfo, updateFirstName, updateLastName, updatePassword, sendRequestToBecomeCreator} = require("../../app/services/userService");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {user1} = require("../userInfo");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {UserDoesntExistError} = require("../../app/error/CustomError");
const {ROLE} = require("../../app/models/role")

describe("test user services", function(){
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await User.deleteMany({})
        await AdminRequest.deleteMany({})

        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
        } catch(error) {
            console.log(error)
        }
    })
    after(async ()=> {
        mongoose.disconnect()
    });

    describe("test getUserInfo service", () => {    
        it("should return the user informations", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await getUserInfo(user._id)
                expect(value).to.have.property("firstName", user1.firstName)
                expect(value).to.have.property("lastName", user1.lastName)
                expect(value).to.have.property("email", user1.email)
                expect(value).to.have.property("birthdate")
                expect(value).to.have.property("password")
                expect(value).to.have.property("verified", true)
                expect(value).to.have.property("tokens")
                expect(value).to.have.property("createdAt")
                expect(value).to.have.property("role", ROLE.member)
                expect(value).to.have.property("verifiedAt")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not return the user informations and throw an exception UserNotAuthorizedError", async () => {
            try {
                let value = await getUserInfo( mongoose.Types.ObjectId())
                expect(value).to.not.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });

    describe("test update password service", () => {    
        it("should update the user password", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updatePassword("password4", user._id)
                expect(value).to.be.equal("Password updated")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not update the user password and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updatePassword("password4", mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });

    describe("test update first name service", () => {    
        it("should update the user first name", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updateFirstName("uchiha", user._id)
                expect(value).to.be.equal("First name updated")
            } catch(error) {
                expect(error).to.not.be.null
            }
        });

        it("should not update the user first name and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updateFirstName("uchiha", mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });

    describe("test update lastName service", () => {    
        it("should update the user last name", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updateLastName("itachi", user._id)
                expect(value).to.be.equal("Last name updated")
            } catch(error) {
                expect(error).to.not.be.null
            }
        });

        it("should not update the user last name and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await updateLastName("itachi", mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });

    describe("test sendCreator request service", () => {    
        it("should send the creator request and save it to the database", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await sendRequestToBecomeCreator(user._id)
                console.log(value)
                expect(value).to.be.equal("Email sent")
            } catch(error) {
                expect(error).to.not.be.null
            }
        });

        it("should not send the creator request and throw an exception UserNotAuthorizedError", async () => {
            try {
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await sendRequestToBecomeCreator(mongoose.Types.ObjectId())
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
    });
});
