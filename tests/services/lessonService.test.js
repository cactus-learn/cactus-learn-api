const expect = require("chai").expect;
const mongoose = require('mongoose');
const Lesson = require("../../app/models/lesson");
const Post = require("../../app/models/post")
const User = require("../../app/models/user")
const Organisation = require("../../app/models/organisation")
const Groupe = require("../../app/models/groupe")
const {createGrp} = require("../../app/services/groupeService");
const {user1, user2, user3, user5} = require("../userInfo");
const {registerUser, verifyUser} = require("../../app/services/authService");
const {updateUserRoleToCreator} = require("../../app/services/adminRequestService");
const {sendRequestToBecomeCreator} = require("../../app/services/userService");
const {create} = require("../../app/services/organisationService")
const {createLesson, updateLesson, deleteLesson} = require("../../app/services/lessonService");
const { DB_URI_TEST } = require('../../config/config');
const env = process.env.NODE_ENV || 'development';
const {ROLE} = require("../../app/models/role")
const {GroupeError, OrganisationError,LessonError, UserDoesntExistError, UserError, UserNotAuthorizedError } = require('../../app/error/CustomError');

describe("test lesson services", function() {
    this.timeout(150000)
    before(async function(){
        //this.enableTimeouts(false)
        /*await mongoose.connect(DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });*/

        await Lesson.deleteMany({})
        await Post.deleteMany({})
        await User.deleteMany({})
        await Organisation.deleteMany({})
        await Groupe.deleteMany({})


        try {
            await registerUser(user1.firstName, user1.lastName, user1.email, user1.birthdate, user1.password);
            let user1Unverified = await User.findOne({email: user1.email.toLowerCase()})
            await verifyUser(user1Unverified.tokens[user1Unverified.tokens.length - 1].token);
            sendRequestToBecomeCreator(user1Unverified._id)

            await registerUser(user2.firstName, user2.lastName, user2.email, user2.birthdate, user2.password);
            let user2Unverified = await User.findOne({email: user2.email.toLowerCase()})
            await verifyUser(user2Unverified.tokens[user2Unverified.tokens.length - 1].token);

            await registerUser(user3.firstName, user3.lastName, user3.email, user3.birthdate, user3.password);
            let user3Unverified = await User.findOne({email: user3.email.toLowerCase()})
            await verifyUser(user3Unverified.tokens[user3Unverified.tokens.length - 1].token);

            await registerUser(user5.firstName, user5.lastName, user5.email, user5.birthdate, user5.password);
            let user5Unverified = await User.findOne({email: user5.email.toLowerCase()})
            await verifyUser(user5Unverified.tokens[user5Unverified.tokens.length - 1].token);
            user5Unverified.role = ROLE.admin;
            user5Unverified.save()
            updateUserRoleToCreator(user5Unverified._id, user1Unverified._id)

            await create("Organisation1", user1Unverified._id)
            let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
            let body = {organisationId: organisation._id, name: "Groupe1", description: "groupe de test"}
            await createGrp(user1Unverified._id, body);
        } catch(error) {
            console.log(error)
        }


    })
    after(async ()=> {
        //mongoose.disconnect()
    });

    describe("test create lesson service", () => {    
        it("should create a lesson and save it the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let chapters = {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}
                let value = await createLesson("Lesson1", user._id, chapters, groupe._id);
                expect(value).to.have.property("title","Lesson1")
                expect(value).to.have.property("creator", user._id)
                expect(value).to.have.property("groupeId", groupe._id)
                expect(value).to.have.property("chapters")
            } catch(error) {
                expect(error).to.be.null
            }
        });

        it("should not create a lesson and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let chapters = {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}
                let value = await createLesson("Lesson1", mongoose.Types.ObjectId(), chapters, groupe._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not create a lesson and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let chapters = {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}
                let value = await createLesson("Lesson1", user._id, chapters, groupe._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User not authorized")
            }
        });
        
        it("should not create a lesson and throw an exception LessonError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await createLesson("Lesson1", user._id, organisation._id,  groupe._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(LessonError)
                expect(error.message).to.be.equal("Lesson already existing in this groupe")
            }
        });
    });


    describe("test update lesson service", () => {    
        it("should not update a lesson in the database and throw an exception LessonError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let lesson = await Lesson.findOne({title: "Lesson1", groupeId: groupe._id})
                let body = {_id: mongoose.Types.ObjectId(), creator: user._id, groupeId: groupe._id, title:"Lesson1Bis", chapters: {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}}
                let value = await updateLesson(user._id, lesson._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(LessonError)
                expect(error.message).to.be.equal("Data inconsistency, id doesnt match")
            }
        });
        
        it("should not update a lesson in the database and throw an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let lesson = await Lesson.findOne({title: "Lesson1", groupeId: groupe._id})
                let body = {_id: lesson._id, creator: user._id, groupeId: groupe._id, title:"Lesson1Bis", chapters: {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}}
                let value = await updateLesson(mongoose.Types.ObjectId(), lesson._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });

        it("should not update a lesson in the database and throw an exception LessonError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let lesson = await Lesson.findOne({title: "Lesson1", groupeId: groupe._id})
                let lessonId = mongoose.Types.ObjectId()
                let body = {_id: lessonId, creator: user._id, groupeId: groupe._id, title:"Lesson1Bis", chapters: {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}}
                let value = await updateLesson(user._id, lessonId, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(LessonError)
                expect(error.message).to.be.equal("Lesson not found")
            }
        });

        it("should not update a lesson in the database and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let lesson = await Lesson.findOne({title: "Lesson1", groupeId: groupe._id})
                let body = {_id: lesson._id, creator: user._id, groupeId: groupe._id, title:"Lesson1Bis", chapters: {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}}
                let value = await updateLesson(user._id, lesson._id, body);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User not authorized")
            }
        });

        it("should update a lesson in the database", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let lesson = await Lesson.findOne({title: "Lesson1", groupeId: groupe._id})
                let body = {_id: lesson._id, creator: user._id, groupeId: groupe._id, title:"Lesson1Bis", chapters: {title: "chapter1", content: "chapitre test", files: [{src:"file", name:"test", type:"txt"}]}}
                let value = await updateLesson(user._id, lesson._id, body);
                expect(value).to.be.equal("Lesson updated.")
            } catch(error) {
                expect(error).to.be.null
            }
        });

    })

    describe("test delete lesson service", () => {    
        it("should not delete a lesson from the database and thrown an exception UserDoesntExistError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let lesson = await Lesson.findOne({title: "Lesson1Bis", groupeId: groupe._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await deleteLesson(mongoose.Types.ObjectId(), lesson._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserDoesntExistError)
                expect(error.message).to.be.equal("User does not exist.")
            }
        });
        
        it("should not delete a lesson from the database and throw an exception LessonError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let lesson = await Lesson.findOne({title: "Lesson1Bis", groupeId: groupe._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await deleteLesson(user._id, mongoose.Types.ObjectId());
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(LessonError)
                expect(error.message).to.be.equal("Lesson not found")
            }
        });

        it("should not delete a lesson from the database and throw an exception UserNotAuthorizedError", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let lesson = await Lesson.findOne({title: "Lesson1Bis", groupeId: groupe._id})
                let user = await User.findOne({email: user2.email.toLowerCase()})
                let value = await deleteLesson(user._id, lesson._id);
                expect(value).to.be.null
            } catch(error) {
                expect(error).to.be.instanceOf(UserNotAuthorizedError)
                expect(error.message).to.be.equal("User not authorized")
            }
        });

        it("should delete a lesson from the database and return a message", async () => {
            try {
                let organisation = await Organisation.findOne({name: "Organisation1".toLowerCase()})
                let groupe = await Groupe.findOne({organisationId: organisation._id, name: "Groupe1"})
                let lesson = await Lesson.findOne({title: "Lesson1Bis", groupeId: groupe._id})
                let user = await User.findOne({email: user1.email.toLowerCase()})
                let value = await deleteLesson(user._id, lesson._id);
                expect(value).to.be.equal("Lesson deleted.")
            } catch(error) {
                expect(error).to.be.null
            }
        });
    })
});
