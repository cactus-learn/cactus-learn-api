const express = require('express');
const path = require("path")
const {PORT} = require("./config/config");
const connectToMongoDB = require('./config/dbConnect');
const {getAllMessagesBetweenTwoMembers} = require("./app/services/messageService")
const User = require("./app/models/user")
const Message = require("./app/models/message")
const ejs = require("ejs");
const app = express();
const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer, {
    perMessageDeflate: false,
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
      allowedHeaders: ["my-custom-header"],
      credentials: true
    }
  });
var cors = require('cors');
const { isObject } = require('util');

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Accept request
app.use(express.json({extended: false}));

app.set("view engine", "ejs");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"),
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Access-Control-Allow-Headers, Accept, Authorization"),
    next()
})

// Connection to MongoDB
connectToMongoDB();

// Routes
app.use('/auth', require('./routes/authRoutes'));
app.use('/password', require('./routes/ForgotPasswordRoutes'));
app.use('/user', require('./routes/userRoutes'));
app.use('/adminRequest', require('./routes/adminRequestRoutes'));
app.use('/message', require('./routes/messageRoutes'))
app.use('/exercise', require('./routes/exerciseRoutes'));
app.use('/organisation', require('./routes/organisationRoutes'));
app.use('/lesson', require('./routes/lesson'));
 
app.use(cors());

io.on('connection', socket => {
    const onNewMessage = async(message) => {
        senderId = message.sender
        receiverId = message.receiverId
        content = message.content
    
        let user = await User.findById(senderId);
        let receiver = await User.findById(receiverId);
    
        let receiverInfo = {firstName: receiver.firstName, lastName: receiver.lastName, email: receiver.email, birthdate: receiver.birthdate}
        let newMessage = new Message({content: content, receiverId: receiverId, receiverInfo: receiverInfo,  sender: user._id});
        newMessage.save()
        
        socket.emit("receive_message", newMessage)
        socket.to(receiverId).emit("receive_message", newMessage)
    }
    
    const onUsers = async(users) => {
        const messages = await getAllMessagesBetweenTwoMembers(users.receiverId, users.senderId)
        socket.emit("chatMessages", messages)
    }

    
    chatId = socket.handshake.query.chatId
    console.log("New ws connection....")
    //console.log(chatId)
    socket.join(chatId)

    socket.on("disconnection", () => {
        socket.leave(chatId)
        socket.removeAllListeners();
        console.log("Déconnexion")
    })
    
    socket.off("new_message", onNewMessage).on("new_message", onNewMessage)
    socket.off("users", onUsers).on("users", onUsers)
})

// Run the server
httpServer.listen(PORT, function() {
    console.log(`App running on port ${PORT}`);
});

module.exports.app = app;
