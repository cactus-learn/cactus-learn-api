const mongoose = require("mongoose");

const groupe = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255,
        trim: true,
    },
    description: {
        type: String,
    },
    organisationId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Organisation',
        required: true,
    },
    creatorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    students: [
        { userId : {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        _id: false,
    }],
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    lessonId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Lesson'
    },
    exercises: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Exercise',
        default: []
    }]
});

module.exports = mongoose.model("Groupe", groupe);