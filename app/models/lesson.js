const mongoose = require("mongoose");

const lesson = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    groupeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Groupe'
    },
    chapters: [{
        title: {
            type: String,
            required: true,
        },
        content: {
            type: String,
            required: true,
        },
        file: {
            src : {
                type: String,
            },
            name: {
                type: String,
            },
            type: {
                type: String,
            }
        }
    }],
})

module.exports = mongoose.model("Lesson", lesson);