const mongoose = require("mongoose");

const post = new mongoose.Schema({
    forumId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    }, 
    creatorId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    content: {
        type: String,
        required: true,
        trim: true,
    },
    thumb_up: [
        { userId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false, 
    }],
    thumb_down: [
        { userId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false, 
    }],
    createdAt: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("Post", post);