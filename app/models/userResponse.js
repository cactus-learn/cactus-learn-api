const mongoose = require("mongoose");
const validator = require("validator");

const userResponseSchema = new mongoose.Schema({
    exerciseId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Exercise'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    marks: {
        type: Number
    },
    coefficient: {
        type: Number,
        required: true
    },
    total: {
        type: Number,
        required: true
    },
    sendAt: {
        type: Date,
        default: Date.now()
    },
    questions: [{
        text: {
            type: String,
            required: true
        },
        questionType: {
            type: String,
            required: true
        },
        coefficient: {
            type: Number,
            required: true
        },
        comment: {
            type: String
        },
        responses: [{
            text: {
                type: String,
                required: true
            },
            isUserResponse: {
                type: Boolean,
                required: true
            },
            isAnswer: {
                type: Boolean,
                required: true
            },
            compositionResponse:{
                type: String
            }
        }],
    }],
});


module.exports = mongoose.model("UserResponse", userResponseSchema);