const mongoose = require("mongoose");
const validator = require("validator");

const exerciseSchema = new mongoose.Schema({
    creator : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String,
        required: true
    },
    coefficient: {
        type: Number,
        required: true
    },
    total: {
        type: Number,
        required: true
    },
    questions: [{
        text: {
            type: String,
            required: true
        },
        questionType: {
            type: String,
            required: true
        },
        coefficient: {
            type: Number,
            required: true
        },
        responses: [{
            text: {
                type: String,
                required: true
            },
            isAnswer: {
                type: Boolean,
                required: true
            },
        }],
    }],
    groupeId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Groupe',
        required: true
    },
    startDate:{
        type: Date
    },
    endDate:{
        type: Date
    }
});


module.exports = mongoose.model("Exercise", exerciseSchema);