const mongoose = require("mongoose");
const validator = require("validator");
const jwt = require("jsonwebtoken");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255,
        trim: true,
    },
    lastName: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        minlength: 5,
        maxlength: 255,
    },
    birthdate: {
        type: Date,
        required: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 255,
    },
    role: {
        type: String,
        required: true,
    }, 
    verified: {
        type: Boolean,
        default: false,
    },
    verifiedAt: {
        type: Date,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    tokens: [{
        token: {
            type: String,
            required: true,
        },
        _id: false,
    }]
});

userSchema.methods.generateAuthToken = async function(expirationTime) {
    const user = this;
    const token = jwt.sign({_id: user._id}, process.env.JWT_KEY, {expiresIn: expirationTime })
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}

module.exports = mongoose.model("User", userSchema);