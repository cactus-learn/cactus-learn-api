const mongoose = require("mongoose");
const Post = require("./post");

const adminRequest = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    granted: {
        type: Boolean,
        default: false,
    },
    grantedAt: {
        type: Date,
    },
    grantedBy: {
        type: mongoose.Schema.Types.ObjectId,
    },
    deniedAt: {
        type: Date,
    },
    deniedBy: {
        type: mongoose.Schema.Types.ObjectId,
    },
    updatedAt: {
        type: Date,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("AdminRequest", adminRequest);