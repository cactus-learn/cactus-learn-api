const mongoose = require("mongoose");

const message = new mongoose.Schema({
    content: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255,
    },
    receiverId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    receiverInfo: {
        type: Object,
        required: true,
    },
    sender: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    read: {
        type: Boolean,
        required: true,
        default: false,
    },
    isLiked: {
        type: Boolean,
        required: true,
        default: false,
    },
    sentAt: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("Message", message);