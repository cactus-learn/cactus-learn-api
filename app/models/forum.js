const mongoose = require("mongoose");

const forum = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255,
        uppercase: true,
    },
    groupeId: {
        type: mongoose.Schema.Types.ObjectId
    },
    organisationId: {
        type: mongoose.Schema.Types.ObjectId
    },
    creatorId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    posts: [
        { postId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false,
    }],
    statut_close: {
        type: Boolean,
        required: true,
        default: false,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("Forum", forum);