const mongoose = require("mongoose");

const organisation = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        uniqure: true,
        lowercase: true,
        minlength: 2,
        maxlength: 255,
        trim: true,
    },
    creatorId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    admins: [
        { userId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false, 
    }],
    volunteers: [
        { userId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false,
    }],
    members: [
        { userId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false,
    }],
    groupes: [
        { groupeId : {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        _id: false,
    }],
    createdAt: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("Organisation", organisation);