const ROLE = {
    creator: 'creator',
    admin: 'admin',
    member: 'member'
}

module.exports = {
    ROLE: ROLE
}