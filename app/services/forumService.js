const User = require("../models/user");
const Forum = require("../models/forum");
const Post = require("../models/post");
const {getUserDetails} = require("./userService")
const {UserDoesntExistError, ForumError, UserNotAuthorizedError } = require('../error/CustomError');


exports.createForum = async(forumName, groupeId, organisationId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    try{
        if(groupeId == "" || organisationId == ""){
            let forum = await Forum.findOne({name: forumName.toLowerCase(), creatorId: userId})
            if (forum) throw new  ForumError("You already created this forum.")

            let newForum = new Forum({name: forumName.toLowerCase(), creatorId: userId})
                
            return newForum.save().then(async function(newForum){
                const result = await new Promise((resolve, reject) => {
                    resolve(newForum);
                }); 
                return result;
            })
        }

        let forum = await Forum.findOne({name: forumName.toLowerCase(), groupeId: groupeId, organisationId: organisationId})
        if (forum) throw new  ForumError("The forum already exist.")
    
        let newForum = new Forum({name: forumName.toLowerCase(), groupeId: groupeId,  
            organisationId: organisationId,  creatorId: userId});
    
        return newForum.save().then(async function(newForum){
            const result = await new Promise((resolve, reject) => {
                resolve(newForum);
            }); 
            return result;
        })
    }catch(err){
        throw err
    }
}


exports.getForumById = async(forumId) => {
    let forum = await Forum.findById(forumId)
    if (!forum) throw new ForumError("Forum not found")
    else{            
        let posts = []

        for(let postId of forum.posts){
            let post = await Post.findById(postId.postId)
            posts.push(post)
        }
        return {
            _id: forum._id,
            statut_close: forum.statut_close,
            createdAt: forum.createdAt,
            name: forum.name,
            groupeId: forum.groupeId,
            organisationId: forum.organisationId,
            creatorId: forum.creatorId,
            posts: posts,
        }
    }
}

exports.deleteForum = async(forumId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let forum = await Forum.findById(forumId)
    if (!forum) throw new ForumError("This forum does not exist")

    if (forum.creatorId.toString() !== userId.toString()) throw new UserNotAuthorizedError("You must be the creator of the forum")
    
    return Forum.findByIdAndDelete(forumId).then(async (forumId) => {
        const result = await new Promise((resolve, reject) => {
            Post.deleteMany({forumId:forumId}).then(async() => {
                resolve("The forum has been deleted");
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.closeForum = async(forumId, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError
    
    let forum = await Forum.findById(forumId)
    if (!forum) throw new ForumError("This forum does not exist")

    if (forum.creatorId.toString() !== userId.toString()) throw new UserNotAuthorizedError("You must be the creator of the forum")

    forum.statut_close = true;
    return forum.save().then(async () => {
        const result = await new Promise((resolve, reject) => {
            resolve("The forum has been closed");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.getAllPostForForum = async(forumId) => {
    let forum = await Forum.findById(forumId)
    if (!forum) throw new ForumError("Forum not found")

    let posts = await Post.find({forumId: forumId}).sort({createdAt: -1})
    if (!posts || posts.length === 0) return []
    else return posts

}

exports.getAllForumForGroupe = async(groupeId) => {
    let forums = await Forum.find({groupeId:groupeId})
    if (!forums || forums.length === 0) return []

    else return forums

}

exports.getAllForumWithoutGroupe = async() => {
    let forums = await Forum.find({groupeId: undefined})
    if (!forums || forums.length === 0) return []
    else return forums

}


exports.openForum = async(forumId, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    let forum = await Forum.findById(forumId)
    if (!forum) throw new ForumError("This forum does not exist")

    if (!forum.creatorId.equals(userId)) throw new UserNotAuthorizedError("You must be the creator of the forum")

    forum.statut_close = false;
    return forum.save().then(async () => {
        const result = await new Promise((resolve, reject) => {
            resolve("The forum has been opened");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.createPost = async(forumId, content, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let forum = await Forum.findById(forumId)
    if(!forum) throw new ForumError("The forum does not exist.")

    if (forum.statut_close) throw new ForumError("The forum is closed.")
    
    let newPost = new Post({forumId: forumId, creatorId: userId, content: content});
    return newPost.save().then(async (newPost) => {
        const result = await new Promise((resolve, reject) => {
            forum.posts = forum.posts.concat({postId: newPost._id})
            forum.save().then(async() => {
                let userInfo = await getUserDetails(newPost.creatorId)
                resolve({post: newPost, userInfo: userInfo});
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.ratePost = async(postId, positif, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let post = await Post.findById(postId)
    if (!post) throw new ForumError("The post does not exist")
    
    let voteUp = await Post.findOne({_id: post._id, 'thumb_up.userId': userId})
    let voteDown = await Post.findOne({_id: post._id, 'thumb_down.userId': userId})
    
    
    if ((voteUp && positif) || (voteDown && !positif)) throw new ForumError("Already voted")
    if (voteUp)  {
        if (!positif) {
            post.thumb_up.pull({userId: userId})
            post.thumb_down = post.thumb_down.concat({userId: userId})
        }
    } 
    else if (voteDown) {
        if(positif) {
            post.thumb_down.pull({userId: userId})
            post.thumb_up = post.thumb_up.concat({userId: userId})
        }
    } else {
        if(positif) post.thumb_up = post.thumb_up.concat({userId: userId})
        else post.thumb_down = post.thumb_down.concat({userId: userId})
    }

    return post.save().then(async () => {
        const result = await new Promise((resolve, reject) => {
            resolve("The number of thumb has been raised.");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.getPostById = async(postId) => {
    let post = await Post.findById(postId)
    if (!post) throw new ForumError("Post not found")
    else return post
}
