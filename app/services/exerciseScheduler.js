const Agenda = require('agenda');
const ExerciseService = require("../services/exerciseService");

const agenda = new Agenda({ db: { address: process.env.DB_URI } });

agenda.define("mark student who didn't answer on time with a 0", async (job) => {
    await ExerciseService.getAllUserForExerciseWhereEndDateIsPast()
  });
  

(async function () {
    await agenda.start();
  
    await agenda.every("1 days", "mark student who didn't answer on time with a 0");
  })();