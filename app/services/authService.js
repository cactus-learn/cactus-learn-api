const { success, error } = require("../helpers/responseApi");
const {check} = require("express-validator");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const User = require("../models/user");
const jwt = require("jsonwebtoken");
const {generateVerificationTemplate} = require("../../templates_email/generate_template")
const {sendEmail} = require("../services/mailService");
const { UserError, AuthError, UserNotAuthorizedError, ValidationError, UserDoesntExistError} = require('../error/CustomError');
const {ROLE} = require("../models/role")

exports.registerValidation = [
    check("firstName", "First name is required").not().isEmpty(),
    check("lastName", "Last name is required").not().isEmpty(),
    check("email", "Email is required").not().isEmpty(),
    check("birthdate", "Birthdate is required").not().isEmpty(),
    check("password", "Password is required").not().isEmpty(),
];

exports.loginValidation = [
    check("email", "Email is required").not().isEmpty(),
    check("password", "Password is required").not().isEmpty(),
];

function valideEmail(email) {
    if(!validator.isEmail(email)) return false
    else return true
}

exports.registerUser = async(firstName, lastName, email, birthdate, password) => {
    if (!firstName || firstName.toString() === "") throw new ValidationError("First name is required.")
    if (!lastName || lastName.toString() === "") throw new ValidationError("Last name is required.")
    if (!email || email.toString() === "") throw new ValidationError("Email is required.")
    if (!birthdate || birthdate.toString() === "") throw new ValidationError("Birthdate is required.")
    if (!password || password.toString() === "") throw new ValidationError("Password is required.")

    if (!valideEmail(email)) throw new ValidationError("Email address is not valide.")

    let user = await User.findOne({ email: email.toLowerCase() });
    if (user) throw new AuthError("Email is already use")

    let newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email.toLowerCase(),
        birthdate: birthdate,
        role: ROLE.member
    });

    const hash = await bcrypt.genSalt(10);
    newUser.password = await bcrypt.hash(password, hash);

    const tokenExpiresTime = 60*60*48;
    const token = newUser.generateAuthToken(tokenExpiresTime); 
    
    return token.then(async (value) => {
        const result = await new Promise(async(resolve, reject) => {
            message = await generateVerificationTemplate(newUser.email, newUser.lastName, value)
            await sendEmail(message, async(email_response) => {
                resolve(email_response)
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.verifyUser = async(token) => {
    const user = await User.findOne({'tokens.token': token })
    if (!user) throw new AuthError("Invalid token")

    user.verified = true
    user.verifiedAt = Date.now();
    return user.save().then(async (user) => {
        const result = await new Promise((resolve, reject) => {
            resolve("The user is now verified");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.resendVerificationToken = async(email) => {
    if (!email || email.toString() === "") throw new ValidationError("Email is required.")

    const user = await User.findOne({email: email.toLowerCase()});
    if (!user) throw new AuthError("User not found")

    const tokenExpiresTime = 60*60*48;
    const token = user.generateAuthToken(tokenExpiresTime);

    return token.then(async (value) => {
        const result = await new Promise(async(resolve, reject) => {
            message = await generateVerificationTemplate(user.email, user.lastName, value)
            await sendEmail(message, async(email_response) => {
                resolve(email_response)
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.loginUser = async(email, password) => {
    if (!password || password.toString() === "") throw new ValidationError("Password is required.")
    if (!email || email.toString() === "") throw new ValidationError("Email is required.")

    if (!valideEmail(email)) throw new AuthError("Email address is not valid.")

    let user = await User.findOne({email: email});
    if (!user) throw new UserDoesntExistError()

    let passwordVerification = await bcrypt.compare(password, user.password);
    if(!passwordVerification) throw new UserError("Invalid credentials")

    if(user && !user.verified) throw new AuthError("Your account is not verify yet.")

    const tokenExpiresTime = 60*60*72;
    const token = user.generateAuthToken(tokenExpiresTime);
    return token.then(async(token) => {
        const result = await new Promise((resolve, reject) => {
            resolve(token);
        });
        return result;
    }).catch((e) => {
        throw e
    });
}


exports.logoutUser = async(userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserError("User not found")

    user.tokens.pullAll();
    return user.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The user has been logout");
        });
        return result;
    }).catch((e) => {
        throw e
    });
}


exports.authUser = async(req, res, next) => {
    try {
        const authorization = req.header("Authorization");

        const splitAuthorizationHeader = authorization.toString().split(" ");

        const bearer = splitAuthorizationHeader[0];
        const token = splitAuthorizationHeader[1];

        if (bearer.toString() !== "Bearer") return res.status(400).json(error("The token type must be a bearer", res.statusCode));

        if (!token) return res.status(404).json(error("No token found."));

        const jwtData = await jwt.verify(token, process.env.JWT_KEY);

        if (!jwtData) return res.status(401).json(error("Unauthorized", res.statusCode));
        req.userId = jwtData._id;

        next();
    } catch (err) {
        res.status(401).json(error("Unauthorized", res.statusCode));
    }
}
 

exports.authAdmin = async(req, res, next) => {
    try {
        let user = await User.findById(req.userId)
        if (!user) return res.status(404).json(error("User not found.", res.statusCode));

        if (user.role.toString() !== ROLE.admin) return res.status(401).json(error("Unauthorized", res.statusCode))

        next();
    } catch(err) {
        res.status(401).json(error("Unauthorized", res.statusCode));
    }
}


exports.authCreator = async(req, res, next) => {
    try {
        let user = await User.findById(req.userId)
        if (!user) return res.status(404).json(error("User not found.", res.statusCode));

        if (user.role.toString() !== ROLE.creator && user.role.toString() !== ROLE.admin) return res.status(401).json(error("Unauthorized", res.statusCode))

        next();
    } catch(err) {
        res.status(401).json(error("Unauthorized", res.statusCode));
    }
}


exports.valideEmail = valideEmail;