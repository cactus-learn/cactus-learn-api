const User = require("../models/user");
const Lesson = require("../models/lesson");
const GroupeService = require("../services/groupeService");
const { LessonError, UserNotAuthorizedError, UserDoesntExistError } = require('../error/CustomError');

exports.createLesson = async(title, creator, chapters, groupeId) => {
    let user = await User.findById(creator)
    if (!user) throw new UserDoesntExistError()

    if(!await GroupeService.isAdmin(groupeId, creator)) throw new UserNotAuthorizedError("User not authorized")

    if(await GroupeService.lessonAlreadyExist(groupeId)) throw new LessonError("Lesson already existing in this groupe")

    const lesson = await new Lesson({
        title: title,
        creator: creator,
        chapters: chapters,
        groupeId: groupeId
    })
    
    try{
        return await lesson.save().then(async(lesson) => {
            const result = await new Promise(async(resolve, reject) => {
                await GroupeService.addLessonToGroupe(groupeId, lesson._id)
                resolve(lesson);
            });
            return result;
        }).catch((e) => {
            throw e
        })
    }catch(err){
        throw err
    }
}

exports.updateLesson = async(userId, lessonId, body) => {
    if(body._id != lessonId) throw new LessonError('Data inconsistency, id doesnt match')

    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    
    let lesson = await Lesson.findById(lessonId)
    if (!lesson) throw new LessonError('Lesson not found')

    if(!await GroupeService.isAdmin(body.groupeId, body.creator)) throw new UserNotAuthorizedError("User not authorized")

    try{
        return await Lesson.findByIdAndUpdate(lessonId, {
            title: body.title,
            chapters: body.chapters,
        }).then(async() => {
            const result = await new Promise(async(resolve, reject) => {
                resolve("Lesson updated.");
            });
            return result;
        }).catch((e) => {
            throw e
        })
    }catch(err){
        throw err
    }
}

exports.deleteLesson = async(userId, lessonId) => {
    try{
        let user = await User.findById(userId)
        if (!user) throw new UserDoesntExistError()
        
        let lesson = await Lesson.findById(lessonId)
        if (!lesson) throw new LessonError('Lesson not found')

        if(!await GroupeService.isAdmin(lesson.groupeId, userId)) throw new UserNotAuthorizedError("User not authorized")

        return await Lesson.findByIdAndDelete(lessonId).then(async() => {
            const result = await new Promise(async(resolve, reject) => {
                await GroupeService.deleteLessonOfGroupe(lesson).then(async() => {
                    resolve("Lesson deleted.");
                })
            });
            return result;
        }).catch((e) => {
            throw e
        })
    }catch(err){
        throw err
    }
}

exports.getLesson = async(userId, lessonId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    
    let lesson = await Lesson.findById(lessonId)
    if (!lesson) throw new LessonError('Lesson not found')

    return lesson
}