const User = require("../models/user");
const bcrypt = require("bcryptjs");
const AdminRequest = require("../models/adminRequest")
const {sendEmail} = require("./mailService");
const {UserDoesntExistError} = require('../error/CustomError');
const {generateNotificationDemandeTemplate} = require("../../templates_email/generate_template")
const OrganisationService = require('./organisationService')


exports.getUserInfo = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    else return user
}

exports.updatePassword = async(newPassword, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const hash = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(newPassword, hash);
    return user.save().then(async() => {
        const result = await new Promise((resolve, reject) => {
            resolve("Password updated")
        })
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.sendRequestToBecomeCreator = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    let adminRequest = await AdminRequest.findOne({user: user._id})
    if (!adminRequest) adminRequest = new AdminRequest({user: user._id})
    return adminRequest.save().then(async () => {
        const result = await new Promise(async(resolve, reject) => {
            message = await generateNotificationDemandeTemplate(user.firstName, user.lastName, user._id)
            await sendEmail(message, async(email_response) => {
                resolve(email_response)
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.updateFirstName = async(newfirstName, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    user.firstName = newfirstName;
    return user.save().then(async () => {
        const result = await new Promise((resolve, reject) => {
            resolve("First name updated")
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.updateLastName = async(newLastName, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    user.lastName = newLastName;
    return user.save().then(async () => {
        const result = await new Promise((resolve, reject) => {
            resolve("Last name updated")
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.getUserDetails = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    return {
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
    }

}

exports.getUserContacts = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    
    let usersId = await OrganisationService.getUserContacts(userId)
    let usersInfo = []
    try{
        for(let userId of usersId){
            usersInfo.push(await this.getUserDetails(userId))
        }
        return usersInfo
    }catch(err){
        throw err
    }
}
