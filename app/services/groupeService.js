const User = require("../models/user");
const Groupe = require("../models/groupe");
const Organisation = require("../models/organisation");
const Lesson = require("../models/lesson")
const ExerciseService = require("../services/exerciseService");
const LessonService = require("../services/lessonService");

const { GroupeError, OrganisationError, UserNotAuthorizedError, UserDoesntExistError, UserError } = require('../error/CustomError');

exports.createGrp = async(userId, body) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let organisation = await Organisation.findById(body.organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")

    const groupe = await Groupe.findOne({name: body.name,  organisationId: body.organisationId})
    // check if the organisation name is already used.
    if (groupe) throw new GroupeError("The groupe name is already taken.")
    
    let newGroupe = new Groupe({
        name: body.name,
        creatorId: userId,
        organisationId: body.organisationId,
        description: body.description,
    })
    return newGroupe.save().then(async function(newGroupe){
        const result = await new Promise((resolve, reject) => {
            organisation.groupes = organisation.groupes.concat({groupeId: newGroupe._id})
            organisation.save().then(async() => {
                resolve(newGroupe);
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.getGroupeById = async(groupeId) => {
    let groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")

    if(!(groupe.lessonId == undefined || groupe.lessonId == "")){
        let lesson = await Lesson.findById(groupe.lessonId)
        if(lesson){
            groupe = {
                _id: groupe._id,
                createdAt: groupe.createdAt,
                exercises: groupe.exercises,
                name: groupe.name,
                creatorId: groupe.creatorId,
                organisationId: groupe.organisationId,
                description: groupe.description,
                students: groupe.students,
                lesson: lesson
            }
        }
    }

    return groupe
}


exports.getCreator = async(groupeId) => {
    let creator = await Groupe.findById(groupeId).select('creatorId')
    if (!creator) throw new UserError("Creator not found")
    return creator
}


exports.getStudents = async(groupeId) => {
    let students = await Groupe.findById(groupeId).select('students')
    if (!students || students.length === 0) return []
    return students
}

exports.getAllGroupeWhereUserIsStudent = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const groupes = await Groupe.find({'students.userId': userId})
    if (!groupes || groupes.length === 0) return []
    return groupes
}


exports.getAllOrganisationGroupe = async(organisationId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()
    
    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")

    let groupes = await Groupe.find({organisationId: organisationId})
    if (!groupes || groupes.length === 0) groupes = []

    let isAdmin = organisation.admins.some(user => user.userId.toString() === userId.toString()) ||
    organisation.volunteers.some(user => user.userId.toString() === userId.toString()) ||
    organisation.creatorId.toString() === userId.toString()

    return {
        _id: organisation._id,
        name : organisation.name,
        groupes : groupes,
        isAdmin: isAdmin
    }
}

exports.getAllGroupeWhereUserIsCreator = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const groupes = await Groupe.find({creatorId: userId})
    if (!groupes || groupes.length === 0) return []
    else return groupes
}


exports.deleteGrp = async(groupeId, userId) => {
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    const groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("The groupe does not exist.")

    let organisation = await Organisation.findById(groupe.organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")


    if (groupe.creatorId.toString() !== userId.toString()) throw new UserNotAuthorizedError("You must be the creator of the groupe.")

    try{
        await LessonService.deleteLesson(userId, groupe.lessonId)
        
        for(let exId of groupe.exercises){
            await ExerciseService.deleteExercise(exId, userId)
        }
    }catch(err){
        console.log(err)
    }
        

    return Groupe.findByIdAndDelete(groupeId).then(async function(){
        const result = await new Promise((resolve, reject) => {
            organisation.groupes.pull({groupeId: groupe._id});
            organisation.save().then(async() => {
                Lesson.findByIdAndDelete(groupe.lessonId).then(async() => {
                    resolve("The groupe has been deleted.");
                })
            })
        });
        return result;
    }).catch((e) => {
        throw e
    }) 
}


exports.transfertGrpOwnership = async(organisationId, groupeId, newOwnerId, userId) => {
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    let newOwner = await User.findById(newOwnerId);
    if (!newOwner) throw new UserError("New owner not found")

    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")

    const groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("The groupe does not exist")
    if (groupe.creatorId.toString() !== userId.toString()) throw new UserNotAuthorizedError("You must be the creator of the groupe.")

    groupe.creatorId = newOwnerId;
    return groupe.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The ownership has been transfered.");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.addLessonToGroupe = async(groupeId, lessonId) => {
    let groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")

    return await Groupe.findByIdAndUpdate(groupeId, {lessonId: lessonId}).then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The lesson has been added.");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.deleteLessonOfGroupe = async(lesson) => {
    let groupe = await Groupe.findById(lesson.groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")
    
    return await Groupe.findByIdAndUpdate(lesson.groupeId, {lessonId: undefined}).then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The lesson has been deleted");
        });
        return result;
    }).catch((e) => {
        throw e
    })

}


exports.addExerciseToGroupe = async(groupeId, exId) => {
    let groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")

    groupe.exercises.push(exId)
    return await Groupe.findByIdAndUpdate(groupeId, {exercises: groupe.exercises}).then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The exercise has been added to the groupe.");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.getExerciseList = async(groupeId) => {
    let groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")

    return groupe.exercises
}


exports.deleteExercise = async(groupeId, exId) => {
    let groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")

    groupe.exercises.filter(ex => ex !== exId)
    return await Groupe.findByIdAndUpdate(groupeId, {exercises: groupe.exercises}).then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The exercise has been added to the groupe.");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.addMemberGrp = async(organisationId, groupeId, newMemberEmail, userId) => {
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let newMember = await User.findOne({email: newMemberEmail.toLowerCase()})
    if (!newMember) throw new UserError("New member not found")

    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")

    let groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("Groupe not found")

    // check if the user is the groupe's creator
    if (groupe.creatorId.toString() !== userId.toString()) throw new UserNotAuthorizedError("You must be the creator of the groupe.")
    
    let newMemberAlreadyPresent = await  Groupe.findOne({_id: groupeId, organisationId: organisationId, 'students.userId': newMember._id});
    if (newMemberAlreadyPresent) throw new UserError("The new members has already already been added.")

    groupe.students = groupe.students.concat({userId: newMember._id});
    return groupe.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The member has been added.");
        });
        return result;
    }).catch((e) => {
        throw e
    });
}


exports.removeMemberGrp = async(organisationId, groupeId, memberId, userId) => {
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    let member = await User.findById(memberId)
    if (!member) throw new UserError("Member not found")

    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("The organisation does not exist")

    const groupe = await Groupe.findById(groupeId)
    if (!groupe) throw new GroupeError("The groupe does not exist")

    if (groupe.creatorId.toString() !== userId.toString()) throw new UserNotAuthorizedError("You must be the creator of the groupe.")
    
    const memberIsPresent = await Groupe.findOne({_id: groupeId, organisationId: organisationId ,'students.userId': memberId });
    if (memberIsPresent) {
        groupe.students.pull({userId: memberId});
        return groupe.save().then(async function(){
            const result = await new Promise((resolve, reject) => {
                resolve("The user has been deleted from the groupe.");
            });
            return result;
        }).catch((e) => {
            throw e
        });
    } else {
        throw new UserError("The user is not part of the groupe.")
    }
}


exports.lessonAlreadyExist = async(groupeId) => {
    let groupe = await Groupe.findById(groupeId)
    if ((!groupe.lessonId) || groupe.lessonId == "") return false
    return true
}


exports.isMember = async(groupeId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let groupe = await Groupe.findById(groupeId);
    if (!groupe) throw new GroupeError("The groupe does not exist.")
    
    let organisation = await Organisation.findById(groupe.organisationId);
    if (!organisation) throw new OrganisationError("The organisation does not exist")

    return groupe.creatorId.toString() === userId.toString() || groupe.students.some(user => user.userId.toString() === userId.toString()) ||
    organisation.members.some(user => user.userId.toString() === userId.toString()) || organisation.creatorId.toString() === userId.toString() ||
    organisation.volunteers.some(user => user.userId.toString() === userId.toString()) ||
    organisation.admins.some(user => user.userId.toString() === userId.toString())
}


exports.isAdmin = async(groupeId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let groupe = await Groupe.findById(groupeId);
    if (!groupe) throw new GroupeError("The groupe does not exist.")

    let organisation = await Organisation.findById(groupe.organisationId);
    if (!organisation) throw new OrganisationError("The organisation does not exist")

    return groupe.creatorId.toString() === userId.toString() ||
        organisation.admins.some(user => user.userId.toString() === userId.toString()) ||
        organisation.volunteers.some(user => user.userId.toString() === userId.toString())
}

exports.getStudentsList = async(groupeId) => {

    let groupe = await Groupe.findById(groupeId);
    if (!groupe) throw new GroupeError("The group does not exist")

    let organisation = await Organisation.findById(groupe.organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")

    return organisation.members
}
