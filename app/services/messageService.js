const User = require("../models/user");
const Message = require("../models/message");
const { MessageError, UserError, UserNotAuthorizedError, UserDoesntExistError } = require('../error/CustomError');

exports.sendMessage = async(receiverId, content, userId) => {
    let user = null;
    let receiver = null;
    try{
        user = await User.findById(userId);
        if (!user) throw new UserDoesntExistError()
    
        receiver = await User.findById(receiverId)
        if (!receiver) throw new UserError("Receiver not found")
    }catch(err){
        throw new UserError("Error while retrieving the user")
    }
    
    let receiverInfo = {firstName: receiver.firstName, lastName: receiver.lastName, email: receiver.email, birthdate: receiver.birthdate}
    let message = new Message({content: content, receiverId: receiverId, receiverInfo: receiverInfo,  sender: userId});
    return message.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("Message sent.");
        });
        return result;
    }).catch((e) => {
        throw e
    });
}

exports.updateReadStatut = async(messageId, userId) => {
    let message = await Message.findById(messageId)
    if (!message) throw new MessageError("Error while retrieving the message")
    
    if (message.sender.equals(userId)) throw new UserNotAuthorizedError("You are the message sender")

    message.read = true
    return message.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("Read statut updated.");
        });
        return result;
    }).catch((e) => {
        throw e
    });
}


exports.updateIsLikedStatut = async(messageId) => {
    let message = await Message.findById(messageId)
    if (!message) throw new MessageError("Error while retrieving the message")

    message.isLiked = true
    return message.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve("The message has been liked.");
        });
        return result;
    }).catch((e) => {
        throw e
    });
}


exports.getAllMessagesBetweenTwoMembers = async(receiverId, userId) => {
    let user = null;
    let receiver = null;
    try{
        user = await User.findById(userId);
        if (!user) throw new UserDoesntExistError()
    
        receiver = await User.findById(receiverId)
        if (!receiver) throw new UserError("Receiver not found")
    }catch(err){
        throw new UserError("Error while retrieving the user")
    }

    let messages = await Message.find({$or: [{receiverId: receiverId, sender: userId}, {receiverId: userId, sender: receiverId}]}).sort({sentAt: -1})
    if (!messages || messages.length === 0) return []
    else return messages
}


exports.getUserConversations = async(userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let receivers = await Message.find().sort({sentAt: -1}).distinct("receiverId", {sender: userId})
    if (!receivers || receivers.length === 0) return []
    else return receivers    
}
