const User = require("../models/user");
const {ROLE} = require("../models/role")
const AdminRequest = require("../models/adminRequest")
const {sendEmail} = require("../services/mailService");
const {generateDroitCreationTemplate} = require("../../templates_email/generate_template")
const {AdminRequestError, UserDoesntExistError, UserError } = require('../error/CustomError');


exports.getAdminCreatorForUser = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserError("User not found")

    let adminRequest = await AdminRequest.find({user: userId})
    if (!adminRequest && adminRequest.length === 0) return []
    else return adminRequest
}


exports.getAllCreatorRequests = async() => {
    let adminRequest = await AdminRequest.find().sort({createdAt: -1})
    if (!adminRequest && adminRequest.length === 0) return []
    else return adminRequest
}


exports.getCreatorRequestById = async(adminRequestId) => {
    let creatorRequest = await AdminRequest.findById(adminRequestId)
    if (!creatorRequest) throw new AdminRequestError("No creator request found")
    else return creatorRequest
}


exports.updateUserRoleToCreator = async(validatorId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserError("User not found")

    let validator = await User.findById(validatorId)
    if (!validator) throw new UserDoesntExistError()

    let adminRequest = await AdminRequest.findOne({user: user._id})
    if (!adminRequest) throw new AdminRequestError("No creator requestion found for the user.")
    adminRequest.granted = true
    adminRequest.grantedAt = Date.now()
    adminRequest.grantedBy = validatorId
    adminRequest.updatedAt = Date.now()

    user.role = ROLE.creator
    /*message = await generateDroitCreationTemplate(user.email, user.firstName, "acceptée.")
    await sendEmail(message, async(email_response) => {
        console.log(email_response)
    })*/
    return user.save().then(async () => {
        const result = await new Promise(async(resolve, reject) => {
            await adminRequest.save().then(async() => {
                resolve("Request granted")
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


exports.denyUserCreatorRequest = async(validatorId, userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserError("User not found")

    let validator = await User.findById(validatorId)
    if (!validator) throw new UserDoesntExistError()

    let adminRequest = await AdminRequest.findOne({user: user._id})
    if (!adminRequest) throw new AdminRequestError("No creator request found for the user.")
    adminRequest.deniedAt = Date.now()
    adminRequest.deniedBy = validatorId
    adminRequest.updatedAt = Date.now()

    /*message = await generateDroitCreationTemplate(user.email, user.firstName, "refusée.")
    await sendEmail(message, async(email_response) => {
        console.log(email_response)
    })*/
    return adminRequest.save().then(async () => {
        const result = await new Promise(async(resolve, reject) => {
            resolve("Request denied")
        });
        return result;
    }).catch((e) => {
        throw e
    })
}
