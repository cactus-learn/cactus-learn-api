const UserResponse = require("../models/userResponse");
const Exercise = require("../models/exercise");
const ExerciseService = require("../services/exerciseService");
const GroupeService = require("../services/groupeService");
const { ExerciseError, UserNotAuthorizedError} = require('../error/CustomError');


async function createUserResponse(userId, body){
    
    let exercise = await Exercise.findById(body.exerciseId)
    if (!exercise) throw new ExerciseError("Exercise not found")
    
    if(await hasUserAlreadyAnswer(userId, body.exerciseId)) throw new ExerciseError("User already answer this question.")

    let userResponse = new UserResponse({
        exerciseId: body.exerciseId,
        questions: body.questions,
        total: exercise.total,
        coefficient: exercise.coefficient,
        userId: userId,
        marks: getUserMarks(body.questions)
    })

    return await userResponse.save().then(async(userResponse) => {
        const result = await new Promise(async(resolve, reject) => {
            resolve(userResponse);
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

async function findbyId(id){
    let userResponse = await UserResponse.findById(id)
    if(!userResponse) throw new ExerciseError("User response not found")
    return userResponse
}

async function findbyExerciseId(exId){
    let exercise = await UserResponse.find({exerciseId: exId})
    if(!exercise) throw new ExerciseError("Exercise doens't exist")

    let userResponses = await UserResponse.find({exerciseId: exId})
    if(!userResponses) return []
    return {
        _id: exercise._id,
        title: exercise.title,
        userResponses: userResponses
    }
}

async function findbyUserIdAndExerciseId(userId, exId){
    let userResponses = await UserResponse.find({userId: userId, exerciseId: exId})
    if(userResponses) return userResponses
}

async function findbyUserId(userId){
    let userResponses = await UserResponse.find({userId: userId})
    if(!userResponses) return []
    return userResponses
}

async function addCorrectionToUserResponse(userId, body){
    let exercise = await ExerciseService.findExerciseById(body.exerciseId)

    if(!await GroupeService.isAdmin(exercise.groupeId, userId)) throw new UserNotAuthorizedError("User can't add a comment")


    let userResponses = await findbyId(body._id)
    if(!userResponses) throw new ExerciseError("User response not found")

    if(userResponses.userId.toString() !== body.userId.toString()) throw new ExerciseError("Data inconsistency : not the same user")
    if(userResponses.exerciseId.toString() !== body.exerciseId.toString()) throw new ExerciseError("Data inconsistency: not the same exercise")

    let questions = body.questions

    for(let i = 0; i < questions.length; i++){
        userResponses.questions.some(question => {
            if(question.text == questions[i].text){
                question.comment = questions[i].comment
            }
        })
    }
    let marks = userResponses.marks
    if(body.marks) {
        if(body.marks <= exercise.total){
            userResponses.marks = body.marks
        }else if(body.marks > exercise.total){
            userResponses.marks = exercise.total
        }else if(body.marks < 0){
            userResponses.marks = 0
        }
    }

    return await UserResponse.findByIdAndUpdate(body._id, 
        {
            $set :{
                questions: questions,
                marks: marks
        }
    }).then(async() => {
        const result = await new Promise(async(resolve, reject) => {
            resolve("Correction added.");
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

async function getUserMarksForGroupe(groupeId, user){
    let groupe = await GroupeService.getGroupeById(groupeId)
    let responses = []
    let totalMarks = 0
    let totalCoeff =0
    for(let exerciseId of groupe.exercises){
        let exercise = await Exercise.findById(exerciseId)
        if(exercise){
            let userResponse = await UserResponse.findOne({userId: user._id, exerciseId: exercise._id})
            if(userResponse){
                let grade = userResponse.marks/exercise.total
                responses.push({
                    exercise:  {
                        _id: exercise._id,
                        title: exercise.title,
                        coefficient: exercise.coefficient,
                        total: exercise.total,
                        marks:  userResponse.marks,
                    },
                    grade: userResponse.marks/exercise.total
                })
                totalMarks+=grade*exercise.coefficient
                totalCoeff+=exercise.coefficient
            }
        }
    }
    if(totalCoeff==0) totalCoeff=1
    return {
        user:{
            _id: user._id,
            lastName: user.lastName,
            firstName: user.firstName,
        },
        responses: responses,
        averageScore: parseFloat(((totalMarks/totalCoeff)*20).toFixed(2))
    }
}

async function hasUserAlreadyAnswer(userId, exerciseId){
    let response = await UserResponse.find({exerciseId: exerciseId, userId: userId})
    if (!response || response.length == 0) return false
    return true
}

function getUserMarks(questions){
    let total = 0
    questions.some(question => {
        let totalGoodAns = getTotalGoodAns(question)
        let goodAns = 0
        let badAns = 0
        for (let response of question.responses){
            if(response.isAnswer && response.isUserResponse){
                goodAns++
            }else if(!response.isAnswer && response.isUserResponse){
                badAns++
            }
        }    
        if(badAns>goodAns) {
            total+=0
        }
        else{
            total += ((goodAns-badAns)/totalGoodAns)*question.coefficient
        }
    })
    return total
}

function getTotalGoodAns(question){
    let totalAns = 0
    for(let j = 0; j < question.responses.length; j++){
        if(question.responses[j].isAnswer){
            totalAns++
        }
    }
    return totalAns
}

async function getUserWhoRespondedToExercise(exerciseId){
    let userResponses = await UserResponse.find({exerciseId: exerciseId})
    if(!userResponses) return []
    return userResponses
}

async function markUsersWhoDidntAnswerExercise(exercise, userIds){
    for(let id of userIds){
        try{
            let userResponse = new UserResponse({
                exerciseId: exercise._id,
                questions: [],
                total: exercise.total,
                coefficient: exercise.coefficient,
                userId: id,
                marks: 0
            })
            await userResponse.save()
        }catch(err){
            console.log("[LOG] Can't mark user "+id +" for exercise "+exercise._id)
        }
    }
}

exports.findbyId = findbyId
exports.findbyExerciseId = findbyExerciseId
exports.findbyUserId = findbyUserId
exports.findbyUserIdAndExerciseId = findbyUserIdAndExerciseId
exports.hasUserAlreadyAnswer = hasUserAlreadyAnswer
exports.createUserResponse = createUserResponse
exports.addCorrectionToUserResponse = addCorrectionToUserResponse
exports.getUserMarksForGroupe = getUserMarksForGroupe
exports.markUsersWhoDidntAnswerExercise = markUsersWhoDidntAnswerExercise