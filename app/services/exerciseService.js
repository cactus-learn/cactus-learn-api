
const User = require("../models/user");
const Exercise = require("../models/exercise");
const UserResponse = require("../models/userResponse");
const Groupe = require("../models/groupe");
const UserResponseService = require("../services/userResponseService");
const GroupeService = require("../services/groupeService");
const OrganisationService = require("../services/organisationService");

const { ExerciseError, UserNotAuthorizedError, UserDoesntExistError } = require('../error/CustomError');
const exercise = require("../models/exercise");

async function createExercise(body, userId){
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    if(!isDatesValid(body.startDate, body.endDate)) throw new ExerciseError("Date is not valid")

    let newExercise = new Exercise({
        creator: userId,
        title: body.title,
        questions: body.questions,
        coefficient: body.coefficient,
        total: getTotalMark(body.questions),
        groupeId: body.groupeId,
        startDate: body.startDate,
        endDate: body.endDate
    })
    try{
        return await  newExercise.save().then(async (newExercise) => {
            const result = await new Promise(async(resolve, reject) => {
                await GroupeService.addExerciseToGroupe(body.groupeId, exercise._id)
                resolve(newExercise);
            });
            return result;
        }).catch((e) => {
            throw e
        })
    }catch(err){
        throw err
    }
}

function isDatesValid(date1, date2){
    let newDate = new Date(Date.now())
    let todayDate = new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate())
    return new Date(date1) > todayDate || new Date(date2) > todayDate
}

function getTotalMark(questions){
    let total = 0
    for(let i = 0; i < questions.length; i++){
        total+= questions[i].coefficient
    }
    return total
}

async function getExerciseById(exId, userId){
    try{
        const user = await User.findById(userId);
        if (!user) throw new UserDoesntExistError()
    
        let exercise = await findExerciseById(exId)
        if (!exercise) throw new ExerciseError("Exercise not found")
        if(!(await GroupeService.isMember(exercise.groupeId, userId))) throw new UserNotAuthorizedError("User not allowed to see the exercise")
    
        return exercise
    }catch(err){
        throw err
    }

}

async function getUserResponseForExercise(exId, userId){
    try{
        const user = await User.findById(userId);
        if (!user) throw new UserDoesntExistError()
    
        let exercise = await findExerciseById(exId)
        if (!exercise) throw new ExerciseError("Exercise not found")

        if(!(await GroupeService.isAdmin(exercise.groupeId, userId))) throw new UserNotAuthorizedError("User not allowed to answer")
        
        let responses = await UserResponse.find({exerciseId: exId})

        return {
            _id: exercise._id,
            title: exercise.title,
            userResponses: responses
        }
    }catch(err){
        throw err
    }

}

async function getMyExercises(userId){
    try{
        const user = await User.findById(userId);
        if (!user) throw new UserDoesntExistError()
    
        let groupes = []

        let organisations = await OrganisationService.getAllOrganisationWhereUserIsMember(userId)

        for(let organisation of organisations){
            groupes.push.apply(groupes, organisation.groupes)
        }
        let exercises = []
        let userExercise = []
        for(let groupeId of groupes){
            try{
                let groupe = await Groupe.findById(groupeId.groupeId)
                if(groupe!=null || groupe!=undefined){
                    exercises = await Exercise.find({groupeId: groupe._id})
                    for(let exercise of exercises){
                        let userResponses = await UserResponseService.findbyUserIdAndExerciseId(userId, exercise._id)
                        exercises.filter(exercise => userResponses.some(response => response.exerciseId.toString() == exercise._id))
                    }
                    userExercise.push({
                        groupeId: groupe._id,
                        groupeName: groupe.name,
                        exercises: exercises
                    })
                }
            }catch(err){
                console.log(err)
            }
        }
        return userExercise
    }catch(err){
        throw err
    }

}

async function getMyResponses(userId){
    try{
        const user = await User.findById(userId);
        if (!user) throw new UserDoesntExistError()
        
        return await getUserReponseWithGroupeInfo(userId)
    }catch(err){
        throw err
    }
}

async function getUserReponseWithGroupeInfo(userId){
    let res = []
    let userResponses = await UserResponse.find({userId: userId})
    for(let response of userResponses){
        try{
            let exercise = await Exercise.findById(response.exerciseId)
            let groupe = await Groupe.findById(exercise.groupeId)
            res.push({
                exercise:{
                    _id: exercise.id,
                    title: exercise.title,
                },
                groupe: {
                    _id: groupe.id,
                    name: groupe.name,
                },
                response: response
            })            
        }catch(err){
            console.log(err)
        }
    }
    return res
}

async function deleteExercise(exId, userId){
    let exercise = await getExerciseById(exId, userId)
    if(!await GroupeService.isAdmin(exercise.groupeId, userId)) throw new UserNotAuthorizedError("User can't delete this exercise")
    
    try{
        return await Exercise.findByIdAndDelete(exId).then(async () => {
            const result = await new Promise(async(resolve, reject) => {
                resolve("Exercice deleted.");
            });
            return result;
        }).catch((e) => {
            throw e
        })
    }catch(err){
        throw err
    }
}

async function findExerciseById(exId){
    let exercise = await Exercise.findById(exId)
    if (!exercise) throw new ExerciseError("Exercise not found")
    return exercise
}

async function findExercisesByGroupe(groupeId){
    return GroupeService.getExerciseList(groupeId)
}

async function getExercisesByGroupe(groupeId, userId){
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    if(!(await GroupeService.isMember(groupeId, userId))) throw new UserNotAuthorizedError("User not allowed to see the exercise")

    let groupe = await Groupe.findById(groupeId)

    if(await OrganisationService.isAdmin(groupe.organisationId, userId)){
        let exercisesList = await Exercise.find({groupeId: groupeId})
        let responses = []
        for(let exercise of exercisesList){
            let res = await UserResponse.find({exerciseId: exercise._id})
            if(res.length>0){
                responses.push(res)
            }
        }
    
        return {
            groupeName: groupe.name,
            exercises : exercisesList,
            responses: responses
        }
    }else{
        let exercises = await getExercisesByGroupeForStudent(groupeId, userId)
        return {
            groupeName: groupe.name,
            exercises : exercises.due,
            responses: exercises.finished

        }
    }

}

async function getExercisesByGroupeForStudent(groupeId, userId){
    let finished = []
    let due = await Exercise.find({groupeId: groupeId})

    for(let ex of due){
        let userResponse = await UserResponse.find({userId: userId, exerciseId: ex._id})
        if(userResponse.length != 0 || !userResponse){
            finished.push(userResponse[0])
        }
    }

    due = due.filter(e => !finished.some(element => element.exerciseId.toString() == e._id))

    return {
        due: due,
        finished: finished
    }
}

async function getAllUserExercises(userId){
    let finished = []
    let due = []
    
    let groupeList = await getGroupeList(userId)

    for(let i = 0; i < groupeList.length; i++){
        let res = await getExercisesByGroupeForStudent(groupeList[i], userId)
        due.push(res.due)
        finished.push(res.finished)
    }
    return {
        due: due,
        finished: finished
    }
}

async function getGroupeList(userId){
    let organisations = await OrganisationService.getAllOrganisationWhereUserIsMember(userId) 
    let groupeList = []
    organisations.some(organisation => {
        organisation.groupes.some(groupe => {
            groupeList.push(groupe.groupeId)
        })
    })
    return groupeList
}

async function updateExercise(body, exId, userId){
    const user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let exercise = await findExerciseById(exId)
    if(exercise.creator.toString() !== userId.toString()) throw new UserNotAuthorizedError("User can't modify this exercise")
    
    try{
        return await Exercise.findByIdAndUpdate(exId, {
            title: body.title,
            questions: body.questions,
            coefficient: body.coefficient,
            total: getTotalMark(body.questions),
            startDate: body.startDate,
            endDate: body.endDate
        }).then(async() => {
            const result = await new Promise(async(resolve, reject) => {
                resolve("Exercice updated.");
            });
            return result;
        }).catch((err) =>{
            throw err
        })
    }catch(err){
        throw err
    }
}

async function addCommentToUserQuestion(exId, userId, body, userResponseId){
    let exercise = getExerciseById(exId)
    if(exercise.creator != userId) throw new UserNotAuthorizedError("User can't add a comment to this exercise")

    try{
        await UserResponseService.addCommentToUserQuestion(userResponseId, body.questions, body.userId)
    }catch(err){
        throw err
    }
}

async function getStudentMarksForGroupe(groupe, user){
    return {
        groupeId: groupe._id,
        groupeName: groupe.name,
        responses : await UserResponseService.getUserMarksForGroupe(groupe._id, user)
    }
}

async function getAllStudentMarksForGroupe(groupe, organisation){
    let sudentsMarks = []


    for(let member of organisation.members){
        let user = await User.findById(member.userId)
        if(user){
            sudentsMarks.push(await UserResponseService.getUserMarksForGroupe(groupe._id, user))
        }
    }
    return {
        groupeId: groupe._id,
        groupeName: groupe.name,
        responses : sudentsMarks
    }
}

async function getGroupeStatistics(groupeId, userId){
    let user = await User.findById(userId)
    if(!user) throw new UserDoesntExistError()

    let groupe = await GroupeService.getGroupeById(groupeId)
    let organisation = await OrganisationService.getOrganisationById(groupe.organisationId)

    if(await OrganisationService.isAdmin(organisation._id, userId)){
        return await getAllStudentMarksForGroupe(groupe, organisation)
    }else{
        return await getStudentMarksForGroupe(groupe, user)
    }

}

async function getAllUserForExerciseWhereEndDateIsPast(){
    let today = new Date(Date.now()).toISOString().substring(0, 10)
    let exercies = await Exercise.find({
        endDate: {$lte: today}
    })

    for(let exercise of exercies){
        let userIds = await GroupeService.getStudentsList(exercise.groupeId)
        let userResponses = await UserResponseService.getUserWhoRespondedToExercise(exercise)
        if(userResponses.length == userIds.length) return "Done"
        let noAnswers = []
        userIds.forEach(el => {
            if(!userResponses.some(val => val.toString() == el.userId)){
                noAnswers.push(el.userId)
            }
        })
        await UserResponseService.markUsersWhoDidntAnswerExercise(exercise, noAnswers)
    }
    return "Done"
}



exports.createExercise = createExercise
exports.getExerciseById = getExerciseById
exports.deleteExercise = deleteExercise
exports.updateExercise = updateExercise
exports.findExerciseById = findExerciseById
exports.findExercisesByGroupe = findExercisesByGroupe
exports.getExercisesByGroupe = getExercisesByGroupe
exports.addCommentToUserQuestion = addCommentToUserQuestion
exports.getAllUserExercises = getAllUserExercises
exports.getUserResponseForExercise = getUserResponseForExercise
exports.getMyExercises = getMyExercises
exports.getMyResponses = getMyResponses
exports.getGroupeStatistics = getGroupeStatistics
exports.getGroupeList = getGroupeList