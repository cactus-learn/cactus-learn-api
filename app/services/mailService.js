const transport = require("../../config/smtpConfig")

exports.sendEmail = async(message, callback) => {
    transport.sendMail(message, function(err, info) {
        if(err) {
            console.log(err)
            return callback("Send mail error")
        } else {
            return callback("Email sent")
        }
    })
}  