const User = require("../models/user");
const {valideEmail} = require("../services/authService")
const {sendEmail} = require("./mailService");
const {generateForgotPasswordTemplate} = require("../../templates_email/generate_template")
const bcrypt = require("bcryptjs");
const {UserError, ValidationError } = require('../error/CustomError');

exports.forgotPassword = async(email) => {
    if (!email || email === "") throw new ValidationError("Email address is required.")

    if (!valideEmail(email))  throw new ValidationError("The email address is not valid")

    const user = await User.findOne({email: email.toLowerCase()});
    if (!user) throw new UserError("User not found")
    
    let temporaryPassword = generateRandomPassword()
    //console.log(temporaryPassword); // à supprimer quand je push
    const hash = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(temporaryPassword, hash);

    return user.save().then(async (user) => {
        const result = await new Promise(async (resolve, reject) => {
            message = await generateForgotPasswordTemplate(user.email, user.firstName, temporaryPassword)
            await sendEmail(message, async(email_response) => {
                resolve(email_response)
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

function generateRandomPassword() {
    let result = "";
    const passwordCharCount = 10
    let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < passwordCharCount; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}