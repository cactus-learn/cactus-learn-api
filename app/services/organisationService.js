const User = require("../models/user");
const Organisation = require("../models/organisation");
const Groupe = require("../models/groupe")
const {ROLE} = require("../models/role");
const {deleteGrp} = require("../services/exerciseService");

const { OrganisationError, UserError, UserDoesntExistError, UserNotAuthorizedError } = require('../error/CustomError');
const organisation = require("../models/organisation");

exports.create = async(organisationName, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let organisation = await Organisation.findOne({name: organisationName.toLowerCase()});
    if (organisation) {
        throw new OrganisationError("The organisation name is already taken.")
    }
    
    let newOrganisation = new Organisation({name: organisationName.toLowerCase(), creatorId: userId})
    return newOrganisation.save().then(async function(newOrganisation){
        const result = await new Promise((resolve, reject) => {
            resolve(newOrganisation);
        });
        return result;
    }).catch((e) => {
        throw e
    })
}


async function getMembers(organisationId) {
    let members = await Organisation.findById(organisationId).select("members")
    if (!members || members.length === 0) return []
    else return members   
}


async function getAdmins(organisationId) {
    let admins = await Organisation.findById(organisationId).select("admins")
    if (!admins || admins.length === 0) return []
    else return admins 
}

async function getVolunteers(organisationId) {
    let volunteers = await Organisation.findById(organisationId).select("volunteers")
    if (!volunteers || volunteers.length === 0) return []
    else return volunteers 
}

exports.getAllUsersFromOrginsationsWhereUserIsPresent = async(userId) => {
    let members = []
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let organisations = await getAllUserOrganisations(userId);
    if (!organisations || organisations.length === 0) return []

    for(let i = 0; i < organisations.length; i++){
        let res = await getAllOrganisationUsers(organisations[i]._id)
        members.push(res)
    }
    return members
}


async function getAllOrganisationUsers(organisationId) {
    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")

    let members = await getMembers(organisationId)
    let admins = await getAdmins(organisationId)
    let volunteers = await getVolunteers(organisationId)

    return {
        "members": members,
        "volunteers": volunteers,
        "admins": admins,
        "creator": organisation.creatorId
    }
}


async function getAllUserOrganisations(userId) {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let members = await Organisation.find({'members.userId': userId})
    let admins = await Organisation.find({'admins.userId': userId})
    let volunteers = await Organisation.find({'volunteers.userId': userId})
    let creator = await Organisation.find({creatorId: userId})

    return members.concat(admins, volunteers, creator)
}


exports.getOrganisationById = async(organisationId) => {
    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("Organisation not found")
    else return organisation
}

exports.getAllOrganisationWhereUserIsMember = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const organisations = await Organisation.find({'members.userId': userId})
    if (!organisations) return []
    return organisations
}

exports.getUserContacts = async(userId) => {
    let organisations = []

    let orgaCreator = await Organisation.find({creatorId: userId})
    let orgaMember = await Organisation.find({'members.userId': userId})
    let orgaAdmin = await Organisation.find({'admins.userId': userId})
    let orgaVolunteer = await Organisation.find({'volunteers.userId': userId})

    if(orgaMember.length > 0) organisations = organisations.concat(orgaMember)
    if(orgaCreator.length > 0) organisations = organisations.concat(orgaCreator)
    if(orgaAdmin.length > 0) organisations = organisations.concat(orgaAdmin)
    if(orgaVolunteer.length > 0) organisations = organisations.concat(orgaVolunteer)
    
    organisations = organisations.filter((v,i,a)=>a.findIndex(t=>(t._id === v._id))===i)
    
    let users = []
    for(let organisation of organisations){
        users.push(organisation.creatorId)
        for(let member of organisation.members){
            users.push(member.userId)
        }
        for(let admin of organisation.admins){
            users.push(admin.userId)
        }  
        for(let volunteer of organisation.volunteers){
            users.push(volunteer.userId)
        }   
    }
    
    users = users.filter(el => el != userId)
    let newArr = []
    users.forEach(el => {
        if(!newArr.some(val => val.toString() == el.toString())){
            newArr.push(el)
        }
    })
    return newArr
}

exports.getAllOrganisationWhereUserIsCreator = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const organisations = await Organisation.find({creatorId: userId})
    if (!organisations || organisations.length === 0) return []
    return organisations
}

exports.getAllOrganisationWhereUserIsAdmin = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const organisations = await Organisation.find({'admins.userId': userId})
    if (!organisations || organisations.length === 0) return []
    return organisations
}

exports.getAllOrganisationWhereUserIsVolunteer = async(userId) => {
    let user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    const organisations = await Organisation.find({'volunteers.userId': userId})
    if (!organisations || organisations.length === 0) return []
    return organisations
}


exports.deleteOrg = async(organisationId, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("The organisation does not exist.")

    let groupes = await Groupe.find({organisationId: organisation._id})

    for(let groupe of groupes){
        try{
            await deleteGrp(groupe._id, userId)
        } catch(err){
            console.log(err)
        }
    }

    try {
        await Organisation.findByIdAndDelete(organisationId)
        return "Organisation deleted"
    }catch(err){
        throw err
    }

}

exports.transfertOwnership = async(organisationId, newOwnerId, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    let newOwner = await User.findById(newOwnerId);
    if (!newOwner) throw new UserError("New owner not found")

    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("The organisation does not exist")

    organisation.creatorId = newOwnerId
    return organisation.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            newOwner.role = ROLE.creator
            user.role = ROLE.member
            newOwner.save().then(async() => {
                user.save().then(async() => {
                    resolve("The ownership has been transfered.");
                })
            })
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.isAdmin = async(organisationId, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()

    let organisation = await Organisation.findById(organisationId)
    if (!organisation) throw new OrganisationError("The organisation does not exist.")

    return organisation.creatorId.toString() === userId.toString() || organisation.admins.includes(userId)
}

exports.addMember = async(organisationId, email, role, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    var newMember = await User.findOne({email: email.toLowerCase()})
    if (!newMember) throw new UserError("New member not found")

    let organisation = await Organisation.findOne({_id: organisationId});
    if (!organisation) throw new OrganisationError("The organisation does not exist")

    if (!organisation.creatorId.equals(userId) && !organisation.admins.includes(userId)) 
        throw new UserNotAuthorizedError("You must be the creator or admin of the organisation.") 

    const newAdminAlreadyPresent = await Organisation.findOne({_id: organisationId, 'admins.userId': newMember._id});
    const newVolunteerAlreadyPresent = await Organisation.findOne({_id: organisationId, 'volunteers.userId': newMember._id });
    const newMemberAlreadyPresent = await Organisation.findOne({_id: organisationId, 'members.userId': newMember._id });
    
    if (newAdminAlreadyPresent) throw new OrganisationError("The new members has already already been added as admin")
    if (newVolunteerAlreadyPresent) throw new OrganisationError("The new members has already already been added as volunteer")
    if (newMemberAlreadyPresent) throw new OrganisationError("The new members has already already been added as member")

    switch(role.toLowerCase()) {
        case "admin": organisation.admins = organisation.admins.concat({userId: newMember._id}); break;
        case "volunteer": organisation.volunteers = organisation.volunteers.concat({userId: newMember._id}); break;
        case "member":organisation.members = organisation.members.concat({userId: newMember._id}); break;
        default: organisation.members = organisation.members.concat({userId: newMember._id}); break;
    }

    return organisation.save().then(async function(){
        const result = await new Promise((resolve, reject) => {
            resolve({
                _id: newMember._id,
                firstName: newMember.firstName,
                lastName: newMember.lastName,
                email: newMember.email,
            });
        });
        return result;
    }).catch((e) => {
        throw e
    })
}

exports.removeMember = async(organisationId, memberId, userId) => {
    let user = await User.findById(userId);
    if (!user) throw new UserDoesntExistError()
    
    var newMember = await User.findById(memberId)
    if (!newMember) throw new UserError("Member not found")

    let organisation = await Organisation.findById(organisationId);
    // check if the organisation exist.
    if (!organisation) throw new OrganisationError("The organisation does not exist")

    if (organisation.creatorId.toString() !== userId.toString() && !organisation.admins.includes(userId)) 
        throw new UserNotAuthorizedError("You must be the creator or admin of the organisation.") 
    
    const newAdminAlreadyPresent = await Organisation.findOne({_id : organisationId, 'admins.userId': memberId });
    const newVolunteerAlreadyPresent = await Organisation.findOne({_id : organisationId, 'volunteers.userId': memberId });
    const newMemberAlreadyPresent = await Organisation .findOne({_id : organisationId, 'members.userId': memberId });
    
    if (newAdminAlreadyPresent) {
        organisation.admins.pull({userId: memberId});
        return organisation.save().then(async function(){
            const result = await new Promise((resolve, reject) => {
                resolve("The user has been deleted from admins.");
            });
            return result;
        }).catch((e) => {
            throw e
        });
    } else if (newVolunteerAlreadyPresent){
        organisation.volunteers.pull({userId: memberId});
        return organisation.save().then(async function(){
            const result = await new Promise((resolve, reject) => {
                resolve("The user has been deleted from volunteers.");
            });
            return result;
        }).catch((e) => {
            throw e
        });
    } else if (newMemberAlreadyPresent){
        organisation.members.pull({userId: memberId});
        return organisation.save().then(async function(){
            const result = await new Promise((resolve, reject) => {
                resolve("The user has been deleted from members.");
            });
            return result;
        }).catch((e) => {
            throw e
        });
    } else {
        throw new UserError("The user is not part of the organisation.")
    }
}

exports.IsMemberOfOrganisation = async(organisationId, userId) => {
    var user = await User.findById(userId)
    if (!user) throw new UserDoesntExistError()

    let organisation = await Organisation.findById(organisationId);
    // check if the organisation exist.
    if (!organisation) throw new OrganisationError("The organisation does not exist.")

    return organisation.creatorId.toString() === userId.toString() || organisation.admins.includes(userId) || organisation.volunteers.includes(userId) || organisation.members.includes(userId)
}


exports.getAllOrganisationUsers = getAllOrganisationUsers
exports.getAllUserOrganisations = getAllUserOrganisations
exports.getMembers = getMembers
exports.getVolunteers = getVolunteers
exports.getAdmins = getAdmins

