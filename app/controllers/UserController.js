const { success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {getUserInfo, updatePassword, sendRequestToBecomeCreator, 
    updateFirstName, updateLastName, getUserDetails, getUserContacts} = require("../services/userService")

const {UserDoesntExistError} = require('../error/CustomError');


exports.getUserInformation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await getUserInfo(req.userId)
        res.status(200).json(result);
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.updateUserPassword = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) { 
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await updatePassword(req.body.newPassword, req.userId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.sendUserRequestToBecomeCreator = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await sendRequestToBecomeCreator(req.userId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.updateUserFirstName = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await updateFirstName(req.body.newfirstName,req.userId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.updateUserLastName = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await updateLastName(req.body.newLastName,req.userId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.getUserDetails = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await getUserDetails(req.params.userId)
        res.status(200).json(result);
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.getAllMyContacts = async(req, res) => {
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        result = await getUserContacts(req.userId)
        res.status(200).json(result);
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}