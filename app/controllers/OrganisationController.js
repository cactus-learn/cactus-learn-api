const { success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {create, deleteOrg, transfertOwnership, addMember, removeMember, 
    getAllOrganisationWhereUserIsAdmin, isAdmin, getAllOrganisationWhereUserIsCreator, getAllOrganisationWhereUserIsMember,
    getAllOrganisationWhereUserIsVolunteer, getOrganisationById, IsMemberOfOrganisation, getAdmins, getVolunteers, getMembers, getAllOrganisationUsers, getAllUserOrganisations} = require("../services/organisationService")

const { OrganisationError, UserError, UserDoesntExistError } = require('../error/CustomError');

exports.createOrganisation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationName} = req.body;
        res.status(201).json(await create(organisationName, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.IsMemberOfOrganisation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId} = req.body;
        res.status(200).json(await IsMemberOfOrganisation(organisationId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getOrganisationById = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getOrganisationById(req.params.organisationId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getMembers = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId} = req.body
        res.status(200).json(await getMembers(organisationId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.isAdmin = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await isAdmin(req.params.organisationId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.getAdmins = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId} = req.body
        res.status(200).json(await getAdmins(organisationId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getVolunteers = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId} = req.body
        res.status(200).json(await getVolunteers(organisationId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllOrganisationWhereUserIsCreator = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllOrganisationWhereUserIsCreator(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllOrganisationWhereUserIsAdmin = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllOrganisationWhereUserIsAdmin(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllOrganisationWhereUserIsVolunteer = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllOrganisationWhereUserIsVolunteer(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllOrganisationWhereUserIsMember = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllOrganisationWhereUserIsMember(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}


exports.deleteOrganisation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId} = req.body;
        res.status(200).json(await deleteOrg(organisationId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.transfertOrganisationOwnership = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId, newOwnerId} = req.body;
        res.status(200).json(await transfertOwnership(organisationId, newOwnerId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.addMemberToOrganisation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId, mail, role} = req.body;
        res.status(200).json(await addMember(organisationId, mail, role, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserError || err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if( err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.removeMemberFromOrganisation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId, memberId} = req.body;
        res.status(200).json(await removeMember(organisationId, memberId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllUsersFromOrginsationsWhereUserIsPresent = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllUsersFromOrginsationsWhereUserIsPresent(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllUserInOrganisation = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const organisationId = req.params.organisationId;
        res.status(200).json(await getAllOrganisationUsers(organisationId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}

exports.getAllUserOrganisations = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const userId = req.userId;
        res.status(200).json(await getAllUserOrganisations(userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserError || err instanceof OrganisationError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));    
        }
    }
}