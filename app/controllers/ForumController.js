const { success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {createForum, createPost, openForum, closeForum, deleteForum, ratePost, getForumById,
     getPostById, getAllPostForForum, getAllForumForGroupe, getAllForumWithoutGroupe} = require("../services/forumService");
const { get } = require("../../config/smtpConfig");
const {UserDoesntExistError, ForumError, UserNotAuthorizedError } = require('../error/CustomError');


exports.createForum = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {forumName, groupeId, organisationId} = req.body;
        res.status(201).json(await createForum(forumName, groupeId, organisationId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getAllPostForForum = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {forumId} = req.body;
        res.status(200).json(await getAllPostForForum(forumId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.getAllForumForGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllForumForGroupe(req.params.groupeId, req.userId));
    } catch(err) {
        console.error(err.message);
        res.status(400).json(error(err.message, res.statusCode));  
    }
}

exports.getAllForumWithoutGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllForumWithoutGroupe());
    } catch(err) {
        console.error(err.message);
        res.status(400).json(error(err.message, res.statusCode));  
    }
}

exports.getForumById = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getForumById(req.params.forumId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getPostById = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getPostById(req.params.postId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.createPost = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {forumId, content} = req.body;
        res.status(200).json(await createPost(forumId, content, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.ratePost = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {postId, positif} = req.body;
        res.status(200).json( await ratePost(postId, positif, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.openForum = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    } 
    try {
        res.status(200).json(await openForum(req.body.forumId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.closeForum = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await closeForum(req.body.forumId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.deleteForum = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    } 
    try {
        res.status(200).json(await deleteForum(req.body.forumId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ForumError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}
