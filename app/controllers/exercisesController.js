const { success, error } = require("../helpers/responseApi");
const ExerciseServcice = require("../services/exerciseService");
const UserResponseService = require("../services/userResponseService");

const { ExerciseError ,UserNotAuthorizedError, UserDoesntExistError, GroupeError } = require('../error/CustomError');

exports.createExercise = async (req, res) => {
    try {
        let createEx = await ExerciseServcice.createExercise(
            req.body,
            req.userId)
        
        res.status(201).json(createEx)
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}

exports.getExercise = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getExerciseById(req.params.id, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}

exports.getUserResponsesForExercise = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getUserResponseForExercise(req.params.exId, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}

exports.updateExercise = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.updateExercise(req.body, req.params.id, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}


exports.deleteExercise = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.deleteExercise(req.params.id, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}


exports.getAllExercises = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getAllUserExercises(req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}


exports.addCorrectionToUserResponse = async (req, res) => {
    try {
        await UserResponseService.addCorrectionToUserResponse(req.userId, req.body)
        res.status(200).json(success("Correction added to the user response"))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}


exports.userRespondToExercise = async (req, res) => {
    try {
        res.status(200).json(await UserResponseService.createUserResponse(req.userId, req.body))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}

exports.getMyExercises = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getMyExercises(req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}

exports.getMyResponses = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getMyResponses(req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}


exports.getExercisesByGroupe = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getExercisesByGroupe(req.params.groupeId, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof ExerciseError){
            res.status(412).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}


exports.getStudentMarksForGroupe = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getStudentMarksForGroupe(req.params.groupeId, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError || err instanceof GroupeError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}

exports.getGroupeStatistics = async (req, res) => {
    try {
        res.status(200).json(await ExerciseServcice.getGroupeStatistics(req.params.groupeId, req.userId))
    } catch(err) {
        console.error(err);
        if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));
        }
        else if(err instanceof UserDoesntExistError || err instanceof GroupeError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else {
            res.status(400).json(error(err.message, res.statusCode));
        }
    }
}