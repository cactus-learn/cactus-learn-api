const {success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {registerUser, verifyUser, resendVerificationToken, loginUser} = require("../services/authService")
const { UserError, AuthError, UserDoesntExistError,UserNotAuthorizedError, ValidationError } = require('../error/CustomError');

exports.register = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const { firstName, lastName, email, birthdate, password } = req.body;
        let result = await registerUser(firstName, lastName, email, birthdate, password)
        res.status(201).json({result});
    } catch (err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError || err instanceof UserError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AuthError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof ValidationError) {
            res.status(401).json(error(err.message, res.statusCode));   
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
};


exports.verify = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        let result = await verifyUser(req.params.token)
        res.redirect(process.env.CACTUS_WELCOME_PAGE_URL)
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AuthError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof ValidationError){
            res.status(401).json(error(err.message, res.statusCode)); 
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.resendVerification = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {email} = req.body;
        let result = await resendVerificationToken(email);
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AuthError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof ValidationError){
            res.status(401).json(error(err.message, res.statusCode)); 
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

 
exports.login = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {email, password } = req.body;
        let token = await loginUser(email, password)
        res.status(200).json({token});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AuthError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof ValidationError){
            res.status(401).json(error(err.message, res.statusCode)); 
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.logout = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {

    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AuthError || err instanceof ValidationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}
