const { success, error } = require("../helpers/responseApi");
const LessonService = require("../services/lessonService");
const { LessonError, UserNotAuthorizedError, UserDoesntExistError } = require('../error/CustomError');

exports.createLesson = async(req, res) => {
    try{
        let createdLesson = await LessonService.createLesson(
            req.body.title,
            req.userId,
            req.body.chapters,
            req.body.groupeId
        )
        res.status(201).json(createdLesson)
    }catch(err){
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof LessonError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.getLesson = async(req, res) => {
    try{
        let lesson = await LessonService.getLesson(
            req.userId,
            req.params.id
        )

        res.status(200).json(lesson)
    }catch(err){
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof LessonError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.updateLesson = async(req, res) => {
    try{
        await LessonService.updateLesson(
            req.userId,
            req.params.id,
            req.body
        )
        
        res.status(200).json(success("The lesson has been updated."))
    }catch(err){
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof LessonError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.deleteLesson = async(req, res) => {
    try{
        await LessonService.deleteLesson(
            req.userId,
            req.params.id
        )

        res.status(200).json(success("The lesson has been deleted."))
    }catch(err){
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof LessonError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}