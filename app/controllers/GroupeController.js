const { success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {createGrp, deleteGrp, transfertGrpOwnership, addMemberGrp, 
    removeMemberGrp, getAllGroupeWhereUserIsCreator, getAllGroupeWhereUserIsStudent, IsMember, getGroupeById, getCreator, getStudents, getAllOrganisationGroupe} = require("../services/groupeService")

const { GroupeError, OrganisationError, UserNotAuthorizedError, UserDoesntExistError, UserError } = require('../error/CustomError');

exports.createGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(201).json(await createGrp(req.userId, req.body));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getCreator = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {groupeId} = req.body;
        res.status(200).json(await getCreator(groupeId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getStudents = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {groupeId} = req.body;
        res.status(200).json(await getStudents(groupeId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getGroupeById = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getGroupeById(req.params.groupeId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getAllGroupeWhereUserIsCreator = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllGroupeWhereUserIsCreator(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getAllGroupeWhereUserIsStudent = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllGroupeWhereUserIsStudent(req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getAllOrganisationGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        res.status(200).json(await getAllOrganisationGroupe(req.params.organisationId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.IsMember = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {groupeId} = req.body;
        res.status(200).json(await IsMember(groupeId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.deleteGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const groupeId = req.params.groupeId;
        res.status(200).json(await deleteGrp(groupeId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.transfertGroupeOwnership = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId, groupeName, newOwnerId} = req.body;
        res.status(200).json(await transfertGrpOwnership(organisationId, groupeName, newOwnerId, req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.addMemberToGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId, groupeName, newMemberId} = req.body;
        res.status(200).json(await addMemberGrp(organisationId, groupeName, newMemberId,  req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.removeMemberFromGroupe = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {organisationId, groupeName, newMemberId} = req.body;
        res.status(200).json(await removeMemberGrp(organisationId, groupeName, newMemberId,  req.userId));
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof GroupeError || err instanceof OrganisationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else if(err instanceof UserNotAuthorizedError){
            res.status(403).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


