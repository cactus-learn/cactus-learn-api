const { success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {forgotPassword} = require("../services/forgotPasswordService")
const {UserError, ValidationError } = require('../error/CustomError');

exports.forgot = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {email} = req.body;
        let result = await forgotPassword(email)
        res.status(200).json({result});
    } catch (err) {
        console.error(err.message);
        if(err instanceof ValidationError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}