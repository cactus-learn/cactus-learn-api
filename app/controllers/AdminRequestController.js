const { success, error, validation } = require("../helpers/responseApi");
const { validationResult } = require("express-validator");
const {getAdminCreatorForUser, getAllCreatorRequests, updateUserRoleToCreator, denyUserCreatorRequest, getCreatorRequestById} = require("../services/adminRequestService")
const { AdminRequestError, UserDoesntExistError, UserError } = require('../error/CustomError');

exports.getCreatorRequestForUser = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        let result = await getAdminCreatorForUser(req.params.userId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AdminRequestError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.getCreatorRequestById = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        let result = await getCreatorRequestById(req.params.organisationId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AdminRequestError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.getAllCreatorRequests = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        let result = await getAllCreatorRequests(req.userId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AdminRequestError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}


exports.updateUserRoleToCreator = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {userRequestedCreatorRoleId} = req.body;
        let result = await updateUserRoleToCreator(req.userId, userRequestedCreatorRoleId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AdminRequestError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}

exports.denyUserCreatorRequest = async(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json(validation(errors.array()));
    }
    try {
        const {userRequestedCreatorRoleId} = req.body;
        let result = await denyUserCreatorRequest(req.userId, userRequestedCreatorRoleId)
        res.status(200).json({result});
    } catch(err) {
        console.error(err.message);
        if(err instanceof UserDoesntExistError){
            res.status(404).json(error(err.message, res.statusCode));
        }
        else if(err instanceof AdminRequestError || err instanceof UserError){
            res.status(412).json(error(err.message, res.statusCode));            
        }
        else{
            res.status(400).json(error(err.message, res.statusCode));            
        }
    }
}
