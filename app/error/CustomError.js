class ExerciseError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class UserNotAuthorizedError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class UserDoesntExistError extends Error {
    constructor() {
        super("User does not exist.");
        this.name = this.constructor.name;
    }
}

class UserError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class OrganisationError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}


class GroupeError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class LessonError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class ForumError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class AuthError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class AdminRequestError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}


class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}



class MessageError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}


module.exports = {
    ExerciseError,
    UserNotAuthorizedError,  
    UserDoesntExistError,
    UserError,
    GroupeError,
    LessonError,
    ForumError,
    OrganisationError,
    AuthError,
    AdminRequestError,
    ValidationError,
    MessageError
  };
