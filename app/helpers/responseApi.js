/**
 * @desc    success response
 * @param   {string} message
 * @param   {object | array} results
 * @param   {number} statusCode
 */
 exports.success = (message, results, statusCode) => {
    return {
      message,
      error: false,
      code: statusCode,
      results
    };
  };
  
  /**
   * @desc    error response
   * @param   {string} message
   * @param   {number} statusCode
   */
  exports.error = (message, statusCode) => {
    const codes = [400, 401, 404, 403, 422, 500];
    const findCode = codes.find((code) => code == statusCode);
  
    if (!findCode) statusCode = 500;
    else statusCode = findCode;
  
    return {
      message,
      code: statusCode,
      error: true
    };
  };
  
  /**
   * @desc    validation response
   * @param   {object | array} errors
   */
  exports.validation = (errors) => {
    return {
      message: "Validation errors",
      error: true,
      code: 422,
      errors
    };
  };