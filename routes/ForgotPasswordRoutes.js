const router = require("express").Router();

const { forgot } = require("../app/controllers/ForgotPasswordController");

// Routes
router.post("/forgot", forgot);
module.exports = router;