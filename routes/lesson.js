const router = require("express").Router();
const {deleteLesson, updateLesson, getLesson, createLesson} = require("../app/controllers/LessonController");

const { authUser, auth} = require("../app/services/authService");

router.post("/create", authUser, createLesson);
router.delete("/:id", authUser, deleteLesson);
router.patch("/:id", authUser, updateLesson);
router.get("/:id", authUser, getLesson);

module.exports = router;