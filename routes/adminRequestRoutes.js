const router = require("express").Router();

const {getAllCreatorRequests, getCreatorRequestForUser, updateUserRoleToCreator, denyUserCreatorRequest} = require("../app/controllers/AdminRequestController");
const {authUser, authAdmin} = require("../app/services/authService")
const ROLE = require("../app/models/role")

// Routes
router.get("/:creatorRequestId", authUser, authAdmin, getAllCreatorRequests);
router.get("/:userId", authUser, authAdmin, getCreatorRequestForUser);
router.put("/updateRoleToCreator", authUser, authAdmin, updateUserRoleToCreator);
router.put("/denyUserCreatorRequest", authUser, authAdmin, denyUserCreatorRequest);

module.exports = router;

