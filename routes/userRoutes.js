const router = require("express").Router();

const { getUserInformation, updateUserPassword, 
    updateUserLastName, updateUserFirstName, sendUserRequestToBecomeCreator, getUserDetails, getAllMyContacts} = require("../app/controllers/UserController");
const {authUser} = require("../app/services/authService")

// Routes
router.get("/info", authUser, getUserInformation);
router.get("/myContacts", authUser, getAllMyContacts);
router.get("/:userId", authUser, getUserDetails);
router.put("/updatePassword", authUser, updateUserPassword);
router.put("/updateLastName", authUser, updateUserLastName);
router.put("/updateFirstName", authUser, updateUserFirstName);  
router.post("/sendCreatorRequest", authUser, sendUserRequestToBecomeCreator);

module.exports = router;