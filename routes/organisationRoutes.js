const router = require("express").Router();

const { createOrganisation, deleteOrganisation, removeMemberFromOrganisation,
    transfertOrganisationOwnership, addMemberToOrganisation, getOrganisationById,
    getAllOrganisationWhereUserIsCreator, getAllOrganisationWhereUserIsAdmin, getAllOrganisationWhereUserIsVolunteer,
    getAllOrganisationWhereUserIsMember, isAdmin, IsMemberOfOrganisation, getAdmins, getMembers, getVolunteers,
     getAllUserInOrganisation, getAllUserOrganisations, getAllUsersFromOrginsationsWhereUserIsPresent } = require("../app/controllers/OrganisationController");
const { createGroupe, deleteGroupe, removeMemberFromGroupe, getAllGroupeWhereUserIsStudent, IsMember,
        transfertGroupeOwnership, addMemberToGroupe, getAllGroupeWhereUserIsCreator, getGroupeById, getCreator, getStudents, getAllOrganisationGroupe} = require("../app/controllers/GroupeController");
const { createForum, createPost, deleteForum, ratePost,openForum, closeForum, getForumById,
     getPostById, getAllPostForForum, getAllForumForGroupe, getAllForumWithoutGroupe} = require("../app/controllers/ForumController");

const { authUser, authCreator, authAdmin} = require("../app/services/authService");

// Routes
router.post("/create", authUser, authCreator, createOrganisation);
router.get("/userCreatorOrganisation", authUser, getAllOrganisationWhereUserIsCreator);
router.get("/userAdminOrganisation", authUser, getAllOrganisationWhereUserIsAdmin);
router.get("/userVolunteerOganisation", authUser, getAllOrganisationWhereUserIsVolunteer);
router.get("/userMemberOrganisation", authUser, getAllOrganisationWhereUserIsMember);
router.get("/:organisationId/organisationUsers", authUser, getAllUserInOrganisation);
router.get("/myOrganisations", authUser, getAllUserOrganisations);
router.get("/IsMember", authUser, IsMemberOfOrganisation);
router.get("/:organisationId/isAdmin", authUser, isAdmin);
router.put("/addMember", authUser, authCreator, addMemberToOrganisation);
router.get("/members", authUser, getMembers);
router.get("/volunteers", authUser, getVolunteers);
router.get("/admins", authUser, getAdmins);
router.delete("/removeMember", authUser, authCreator, removeMemberFromOrganisation);
router.get("/allMyOrganisationsMembers", authUser, getAllUsersFromOrginsationsWhereUserIsPresent);
router.delete("/delete", authUser, authCreator, deleteOrganisation);
router.put("/transfertOwnership", authUser, authCreator, transfertOrganisationOwnership);
router.get("/:organisationId", authUser, getOrganisationById);

router.get("/all-groupe/:organisationId", authUser, getAllOrganisationGroupe);
router.put("/groupe/addMember", authUser, addMemberToGroupe);
router.post("/groupe/create", authUser, createGroupe);
router.get("/groupe/:groupeId", authUser, getGroupeById);
router.put("/groupe/addMember", authUser, addMemberToGroupe);
router.delete("/groupe/removeMember", authUser, removeMemberFromGroupe);
router.get("/groupe/userStudentGroupe", authUser, getAllGroupeWhereUserIsStudent);
router.get("/groupe/userCreatorGroupe", authUser, getAllGroupeWhereUserIsCreator);
router.get("/groupe/IsMember", authUser, IsMember);
router.get("/groupe/creator", authUser, getCreator);
router.get("/groupe/students", authUser, getStudents);
router.delete("/groupe/:groupeId", authUser, deleteGroupe);
router.put("/groupe/transfertOwnership", authUser, transfertGroupeOwnership);

router.post("/groupe/forum/create", authUser, createForum);
router.post("/groupe/forum/createPost", authUser, createPost);
router.get("/groupe/forum/getByGroup/:groupeId", authUser, getAllForumForGroupe);
router.get("/groupe/forum/withoutgroup", authUser, getAllForumWithoutGroupe);
router.get("/groupe/forum/:forumId", authUser, getForumById);
router.delete("/groupe/forum/delete", authUser, deleteForum);
router.get("/groupe/forum/post/:postId", authUser, getPostById);
router.put("/groupe/forum/ratePost", authUser, ratePost);
router.put("/groupe/:groupeId/ratePost", authUser, ratePost);
router.get("/groupe/forum/post/getAllPost", authUser, getAllPostForForum);
router.put("/groupe/forum/close", authUser, closeForum);
router.put("/groupe/forum/open", authUser, openForum);

module.exports = router;