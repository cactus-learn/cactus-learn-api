const router = require("express").Router();

const {sendMessage,  updateIsLikedStatut, updateReadStatut, getAllMessagesBetweenTwoMembers, getUserConversations, chat} = require("../app/controllers/MessageController");
const {authUser, authRole} = require("../app/services/authService")

// Routes
router.get("/chat", chat);
router.post("/send", authUser, sendMessage);
router.put("/updateReadStatut/:messageId", authUser, updateReadStatut);
router.put("/updateLikeStatut/:messageId", authUser, updateIsLikedStatut);
router.get("/allMessagesBetweenMembers", authUser, getAllMessagesBetweenTwoMembers);
router.get("/conversations", authUser, getUserConversations);

module.exports = router;