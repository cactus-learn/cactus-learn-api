const router = require("express").Router();

const { register, verify, resendVerification, login} = require("../app/controllers/AuthController");

const { registerValidation, loginValidation } = require("../app/services/authService");

// Routes
router.post("/register", registerValidation, register);
router.get("/verify/:token", verify);
router.post("/verify/resend", resendVerification); 
router.post("/login", loginValidation, login);

module.exports = router;