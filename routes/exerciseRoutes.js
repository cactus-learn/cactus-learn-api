const router = require("express").Router();

const { createExercise, getExercise, getAllExercises, updateExercise, deleteExercise,
     userRespondToExercise, getExercisesByGroupe, addCorrectionToUserResponse, getGroupeStatistics,
      getUserResponsesForExercise, getMyExercises, getMyResponses} = require("../app/controllers/exercisesController");

const { authUser, authCreator, authAdmin} = require("../app/services/authService");

// Routes
router.post("/create", authUser, authAdmin, createExercise);
router.get("/", authUser, getAllExercises);
router.get("/myExercises", authUser, getMyExercises);
router.get("/myResponses", authUser, getMyResponses);
router.get("/:id", authUser, getExercise);
router.patch("/:id", authUser, authAdmin, updateExercise);
router.delete("/:id", authUser, authAdmin, deleteExercise);
router.post("/send-response", authUser, userRespondToExercise);
router.get("/groupe/:groupeId", authUser, getExercisesByGroupe);
router.get("/responses/:exId", authUser, getUserResponsesForExercise);
router.put("/correction", authUser, authAdmin, addCorrectionToUserResponse);
router.get("/statistics/groupe/:groupeId", authUser, getGroupeStatistics);

module.exports = router;