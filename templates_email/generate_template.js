const path = require("path");
const ejs = require("ejs");

async function generateVerificationTemplate(userEmail, userLastName, token) {
    const template = ejs.renderFile(path.join(__dirname, "account_verification_email.ejs"), {
      user_firstname: userLastName,
      confirm_link: `${process.env.API_HOST}/auth/verify/${token}`
    })
    .then(result => {
        emailTemplate = result;

        const message = {
            from: process.env.CACTUS_TEAM_MAIL,
            to: userEmail,
            subject: "Vérification du compte",
            html: emailTemplate
        }
        return message
    })
    return template
}

async function generateDroitCreationTemplate(userEmail, userFirstName, decision_droit) {
    const template = ejs.renderFile(path.join(__dirname, "creator_right_request_email.ejs"), {
      user_firstname: userFirstName,
      decision_droit: decision_droit
    })
    .then(result => {
        emailTemplate = result;
      
        const message = {
            from: process.env.CACTUS_TEAM_MAIL,
            to: userEmail,
            subject: "Droit de création d'une organisation",
            html: emailTemplate
        }
        return message
    })
    return template
}

async function generateNotificationDemandeTemplate(userFirstName, userLastName, userId) {
    const template = ejs.renderFile(path.join(__dirname, "notification_demande_droit_creation_email.ejs"), {
      first_name: userFirstName,
      last_name: userLastName,
      id_user: userId
    })
    .then(result => {
        emailTemplate = result;
      
        const message = {
            from: process.env.CACTUS_TEAM_MAIL,
            to: process.env.CACTUS_TEAM_MAIL,
            subject: "Nouvelle demande de droit de création",
            html: emailTemplate
        }
        return message
    })
    return template
}


async function generateForgotPasswordTemplate(userEmail, userFirstName, newPassword) {
    const template = ejs.renderFile(path.join(__dirname, "forgot_password_email.ejs"), {
      first_name: userFirstName,
      new_password: newPassword
    })
    .then(result => {
        emailTemplate = result;
      
        const message = {
            from: process.env.CACTUS_TEAM_MAIL,
            to: userEmail,
            subject: "Nouveau Mot de password",
            html: emailTemplate
        }
        return message
    })
    return template
}

exports.generateForgotPasswordTemplate = generateForgotPasswordTemplate
exports.generateVerificationTemplate = generateVerificationTemplate
exports.generateDroitCreationTemplate = generateDroitCreationTemplate
exports.generateNotificationDemandeTemplate = generateNotificationDemandeTemplate