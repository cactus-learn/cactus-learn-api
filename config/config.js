const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  ENV: process.env.NODE_ENV,
  PORT: parseInt(process.env.PORT),
  DB_URI: process.env.DB_URI,
  DB_URI_TEST: process.env.DB_URI_TEST
};